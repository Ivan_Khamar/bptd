﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba1_Generator.HelperClasses
{
    class GenerationVariants
    {
        private readonly ulong _multiplier;
        private readonly ulong _comparisonModule;
        private readonly ulong _startValue;
        private readonly ulong _growth;

        public GenerationVariants(NumbersController options)
        {
            _multiplier = options.Multiplier;
            _comparisonModule = options.ComparisonModule;
            _startValue = options.StartValue;
            _growth = options.Growth;
        }

        public List<ulong> GenerateSequence(ulong count)
        {
            var randomNumbers = new List<ulong>();
            var currentNumber = _startValue;
            randomNumbers.Add(currentNumber);
            for (ulong i = 0; i < count; i++)
            {
                currentNumber = GenerateNext(currentNumber);
                if (i < 10000)
                {
                    randomNumbers.Add(currentNumber);
                }
            }

            return randomNumbers;
        }

        private ulong GenerateNext(ulong current)
        {
            // (a * Xn + c) mod m
            return (_multiplier * current + _growth) % _comparisonModule;
        }

        public ulong GetPeriod()
        {
            var fileName = DateTime.Now.Ticks + "_numbers.txt";
            StreamWriter file = File.CreateText(fileName);
            var currentNumber = _startValue;
            file.Write(currentNumber + " ");

            ulong counter = 0;
            while (true)
            {
                var previous = currentNumber;
                currentNumber = GenerateNext(currentNumber);
                file.Write(currentNumber + " ");
                if (currentNumber == _startValue || previous == currentNumber)
                {
                    Process.Start("notepad.exe", fileName);
                    file.Close();
                    return ++counter;
                }

                counter++;
            }
        }
    }
}
