﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba1_Generator.HelperClasses
{
    class NumbersGenerator
    {
        public static ulong GetPeriod(IList<ulong> sequence)
        {
            ulong counter = 0;
            var unique = new HashSet<ulong>();
            foreach (var number in sequence)
            {
                var isAlreadyExist = !unique.Add(number);
                if (isAlreadyExist) return counter;

                counter++;
            }

            return counter;
        }

        public static long GetPeriod(IList<long> sequence)
        {
            long counter = 0;
            var unique = new HashSet<long>();
            foreach (var number in sequence)
            {
                var isAlreadyExist = !unique.Add(number);
                if (isAlreadyExist) return counter;

                counter++;
            }

            return counter;
        }
    }
}
