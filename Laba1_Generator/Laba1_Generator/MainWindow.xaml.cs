﻿using Laba1_Generator.HelperClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace Laba1_Generator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        /*private NumbersController GetRandomOptionsFormData()
        {
            try
            {
                var multiplier = ulong.Parse(aControl.Text);
                var comparisonModule = ulong.Parse(mControl.Text);
                var startValue = ulong.Parse(startValueControl.Text);
                var growth = ulong.Parse(cControl.Text);

                return new NumbersController(multiplier, comparisonModule, startValue, growth);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Параметри введені некоректно. " + ex.Message);
                return null;
            }
            catch (Exception)
            {
                MessageBox.Show("Параметри введені некоректно.");
                return null;
            }
        }

        private ulong GetQuantityFormData()
        {
            try
            {
                var quantity = ulong.Parse(startNumberControl.Text);
                return quantity;
            }
            catch (Exception)
            {
                MessageBox.Show("Кількість введена некоректно.");
                return 0;
            }
        }

        private void GenerateButton_Click(object sender, RoutedEventArgs e)
        {
            generateButton.IsEnabled = false;
            var randomOptions = GetRandomOptionsFormData();
            var quantity = GetQuantityFormData();

            if (randomOptions == null || quantity == 0)
            {
                generateButton.IsEnabled = true;
                return;
            }

            var random = new GenerationVariants(randomOptions);
            var numbers = random.GenerateSequence(quantity);

            var stringBuilder = new StringBuilder();
            foreach (var number in numbers)
            {
                stringBuilder.Append(number).Append(" ");
            }

            textBlock1.Text = stringBuilder.ToString();
            label1_Copy.Content = $"periodOfSequence: {random.GetPeriod()}";
            generateButton.IsEnabled = true;
        }

        private void showFileButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void comboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int fid = int.Parse(comboBox.SelectedIndex.ToString()) + 1;
            switch (fid)
            {
                case 1:
                    aControl.Text = "32";
                    cControl.Text = "0";
                    mControl.Text = "1023";
                    startValueControl.Text = "2";
                    break;
                case 2:
                    aControl.Text = "243";
                    cControl.Text = "1";
                    mControl.Text = "2048";
                    startValueControl.Text = "4";
                    break;
                case 3:
                    aControl.Text = "1024";
                    cControl.Text = "2";
                    mControl.Text = "4095";
                    startValueControl.Text = "8";
                    break;
                case 4:
                    aControl.Text = "3125";
                    cControl.Text = "3";
                    mControl.Text = "8191";
                    startValueControl.Text = "16";
                    break;
                case 5:
                    aControl.Text = "7776";
                    cControl.Text = "5";
                    mControl.Text = "16383";
                    startValueControl.Text = "32";
                    break;
                case 6:
                    aControl.Text = "8";
                    cControl.Text = "8";
                    mControl.Text = "32767";
                    startValueControl.Text = "64";
                    break;
                case 7:
                    aControl.Text = "27";
                    cControl.Text = "13";
                    mControl.Text = "65535";
                    startValueControl.Text = "128";
                    break;
                case 8:
                    aControl.Text = "64";
                    cControl.Text = "21";
                    mControl.Text = "131071";
                    startValueControl.Text = "256";
                    break;
                case 9:
                    aControl.Text = "125";
                    cControl.Text = "34";
                    mControl.Text = "262143";
                    startValueControl.Text = "512";
                    break;
                case 10:
                    aControl.Text = "216";
                    cControl.Text = "55";
                    mControl.Text = "524287";
                    startValueControl.Text = "1024";
                    break;
                case 11:
                    aControl.Text = "343";
                    cControl.Text = "89";
                    mControl.Text = "1048575";
                    startValueControl.Text = "1";
                    break;
                case 12:
                    aControl.Text = "512";
                    cControl.Text = "144";
                    mControl.Text = "2097151";
                    startValueControl.Text = "3";
                    break;
                case 13:
                    aControl.Text = "729";
                    cControl.Text = "233";
                    mControl.Text = "4194303";
                    startValueControl.Text = "5";
                    break;
                case 14:
                    aControl.Text = "1000";
                    cControl.Text = "377";
                    mControl.Text = "8388607";
                    startValueControl.Text = "7";
                    break;
                case 15:
                    aControl.Text = "1331";
                    cControl.Text = "610";
                    mControl.Text = "16777215";
                    startValueControl.Text = "9";
                    break;
                case 16:
                    aControl.Text = "1728";
                    cControl.Text = "987";
                    mControl.Text = "33554431";
                    startValueControl.Text = "11";
                    break;
                case 17:
                    aControl.Text = "2197";
                    cControl.Text = "1597";
                    mControl.Text = "67108863";
                    startValueControl.Text = "13";
                    break;
                case 18:
                    aControl.Text = "2744";
                    cControl.Text = "2584";
                    mControl.Text = "134217727";
                    startValueControl.Text = "17";
                    break;
                case 19:
                    aControl.Text = "3375";
                    cControl.Text = "4181";
                    mControl.Text = "268435455";
                    startValueControl.Text = "19";
                    break;
                case 20:
                    aControl.Text = "4096";
                    cControl.Text = "6765";
                    mControl.Text = "536870911";
                    startValueControl.Text = "23";
                    break;
                case 21:
                    aControl.Text = "4913";
                    cControl.Text = "10946";
                    mControl.Text = "1073741823";
                    startValueControl.Text = "29";
                    break;
                case 22:
                    aControl.Text = "16807";
                    cControl.Text = "17711";
                    mControl.Text = "2147483647";
                    startValueControl.Text = "31";
                    break;
                case 23:
                    aControl.Text = "65536";
                    cControl.Text = "28657";
                    mControl.Text = "2147483648";
                    startValueControl.Text = "33";
                    break;
                case 24:
                    aControl.Text = "32768";
                    cControl.Text = "46368";
                    mControl.Text = "2147483645";
                    startValueControl.Text = "37";
                    break;
                case 25:
                    aControl.Text = "16384";
                    cControl.Text = "75025";
                    mControl.Text = "2147483641";
                    startValueControl.Text = "41";
                    break;
                default:
                    aControl.Text = "1";
                    cControl.Text = "1";
                    mControl.Text = "1";
                    startValueControl.Text = "1";
                    break;
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {

        }/*/







        List<long> numberList = new List<long>();
        List<long> numberListCopy = new List<long>();
        private long sequenceQuantity;
        private long aValue;
        private long cValue;
        private long mValue;
        private long startValue;
        String filename = "E:\\VSProjects\\BPtD\\Laba1_Generator\\numbers.txt";
        bool isSequenceCreated = false;
        bool isUnique = true;
        int copyIndex = new int();
        String saveText = "";

        long period = new long();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void generateButton_Click(object sender, RoutedEventArgs e)
        {
            //textBlock1.Text.Remove(0,textBlock1.Text.Length);
            sequenceQuantity = long.Parse(startNumberControl.Text);
            aValue = long.Parse(aControl.Text);
            cValue = long.Parse(cControl.Text);
            startValue = long.Parse(startValueControl.Text);
            long rez = new long();
            long nextRez = new long();

            textBlock1.Text = String.Empty;

            /*if (mControl.Value>0)
            {
                mValue = (int)mControl.Value;

                rez = SequenceFormula(aValue, cValue, mValue, startValue);
                numberList.Add(rez);

                for (int i = 0; i < sequenceQuantity-1; i++)
                {
                    nextRez = SequenceFormula(aValue, cValue, mValue, rez);
                    numberList.Add(nextRez);
                    rez = nextRez;
                }
                
            }
            else
            {
                MessageBox.Show("M cannot be 0.");
            }
        */
        
            if (long.Parse(mControl.Text) > 0)
            {
                mValue = long.Parse(mControl.Text);

                numberList.Add(startValue);

                rez = SequenceFormula(aValue, cValue, mValue, startValue);
                numberList.Add(rez);

                for (int i = 0; i < sequenceQuantity; i++)
                {
                    nextRez = SequenceFormula(aValue, cValue, mValue, rez);
                    numberList.Add(nextRez);
                    rez = nextRez;
                }

            }
            else
            {
                MessageBox.Show("M cannot be 0.");
            }

            //textBlock1.Text = "";

            //Random r = new Random();
            //int rInt = r.Next(20, 30);

            /*for (int j = 0; j < 1; j++)
            {
                for (int i = 0; i < numberList.Count; i++)
                {
                    textBlock1.Text += numberList[i] + " ";
                }
            }
        */
            for (int i = 0; i < sequenceQuantity; i++)
            {
                if (i % 1000000 == 0)
                {
                    textBlock1.Text += "Khamar" + " ";
                }
                textBlock1.Text += numberList[i] + " ";
            }

            //for (int i = 0; i < numberList.Count; i++)
            //{
            //  numberListSave.Add(numberList[i]);
            //}

            numberListCopy = numberList.Distinct().ToList();

            //for (int i = 0; i < numberListCopy.Count; i++)
            //{
            //  MessageBox.Show(numberListCopy[i].ToString());
            //}

            period = NumbersGenerator.GetPeriod(numberList);
            numberList.Clear();

            //label1_Copy.Content = "periodOfSequence: " + startNumberControl.Text;

            isSequenceCreated = true;
        }
        private static long SequenceFormula(
            long mult,
            long c,
            long mod,
            long x)
        {
            return (mult * x + c) % mod;
        }

        private void showFileButton_Click(object sender, RoutedEventArgs e)
        {
            saveText = textBlock1.Text; //string.Join(" ", numberListCopy);
                                                  //saveText += " " + numberListCopy[0];

            //foreach (var item in numberListSave)
            //{
            //MessageBox.Show(item.ToString());
            //}



            label1_Copy.Content = "periodOfSequence: " + period;

            if (textBlock1.Text != String.Empty && (int)long.Parse(mControl.Text) != 0 && isSequenceCreated)
            {
                //saveText += numberListCopy[0] + " ";
                //for (int i = 0; i < numberListCopy.Count; i++)
                //{
                    //if (i > 0 && numberListCopy[i - 1] != numberListCopy[i])
                    //{
                    //MessageBox.Show(numberListCopy[i].ToString());
                    //numberListSave.Add(numberListCopy[i]);
                    //saveText += numberListCopy[i].ToString() + " ";
                    //}
                //}

                //for (int i = 0; i < numberListSave.Count; i++)
                //{
                    //MessageBox.Show(numberListSave[i].ToString());
                    
                //}

                File.WriteAllText(filename, saveText);
                MessageBox.Show("Success. Sequence was saved in E:\\VSProjects\\BPtD\\Laba1_Generator\\numbers.txt");
            }
            else
            {
                if (textBlock1.Text == String.Empty)
                {
                    MessageBox.Show("There was no generated sequence.");
                }
                if ((int)long.Parse(mControl.Text) == 0)
                {
                    MessageBox.Show("M cannot be null.");
                }
                if (!isSequenceCreated)
                {
                    MessageBox.Show("At first generate sequence.");
                }
            }
        }

        private void ComboBoxItem_Selected(object sender, RoutedEventArgs e)
        {

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            /*for (int j = 0; j < numberList.Count; j++)
            {
                if (numberList[j].ToString() == numberList[0].ToString())
                {
                    //MessageBox.Show("Copy spotted on " + j + " element");
                    isUnique = false;
                    copyIndex = j;
                }
                else
                {
                    //MessageBox.Show("All numbers of sequence is unique");
                    isUnique = true;
                }
            }

            if (!isUnique)
            {
                MessageBox.Show("Copy spotted on " + copyIndex + " element");
            }
            else
            {
                MessageBox.Show("All numbers of sequence is unique");
            }
        */ }

        private void comboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            int fid = int.Parse(comboBox.SelectedIndex.ToString()) + 1;
            switch (fid)
            {
                case 1:
                    aControl.Text = "32";
                    cControl.Text = "0";
                    mControl.Text = "1023";
                    startValueControl.Text = "2";
                    break;
                case 2:
                    aControl.Text = "243";
                    cControl.Text = "1";
                    mControl.Text = "2048";
                    startValueControl.Text = "4";
                    break;
                case 3:
                    aControl.Text = "1024";
                    cControl.Text = "2";
                    mControl.Text = "4095";
                    startValueControl.Text = "8";
                    break;
                case 4:
                    aControl.Text = "3125";
                    cControl.Text = "3";
                    mControl.Text = "8191";
                    startValueControl.Text = "16";
                    break;
                case 5:
                    aControl.Text = "7776";
                    cControl.Text = "5";
                    mControl.Text = "16383";
                    startValueControl.Text = "32";
                    break;
                case 6:
                    aControl.Text = "8";
                    cControl.Text = "8";
                    mControl.Text = "32767";
                    startValueControl.Text = "64";
                    break;
                case 7:
                    aControl.Text = "27";
                    cControl.Text = "13";
                    mControl.Text = "65535";
                    startValueControl.Text = "128";
                    break;
                case 8:
                    aControl.Text = "64";
                    cControl.Text = "21";
                    mControl.Text = "131071";
                    startValueControl.Text = "256";
                    break;
                case 9:
                    aControl.Text = "125";
                    cControl.Text = "34";
                    mControl.Text = "262143";
                    startValueControl.Text = "512";
                    break;
                case 10:
                    aControl.Text = "216";
                    cControl.Text = "55";
                    mControl.Text = "524287";
                    startValueControl.Text = "1024";
                    break;
                case 11:
                    aControl.Text = "343";
                    cControl.Text = "89";
                    mControl.Text = "1048575";
                    startValueControl.Text = "1";
                    break;
                case 12:
                    aControl.Text = "512";
                    cControl.Text = "144";
                    mControl.Text = "2097151";
                    startValueControl.Text = "3";
                    break;
                case 13:
                    aControl.Text = "729";
                    cControl.Text = "233";
                    mControl.Text = "4194303";
                    startValueControl.Text = "5";
                    break;
                case 14:
                    aControl.Text = "1000";
                    cControl.Text = "377";
                    mControl.Text = "8388607";
                    startValueControl.Text = "7";
                    break;
                case 15:
                    aControl.Text = "1331";
                    cControl.Text = "610";
                    mControl.Text = "16777215";
                    startValueControl.Text = "9";
                    break;
                case 16:
                    aControl.Text = "1728";
                    cControl.Text = "987";
                    mControl.Text = "33554431";
                    startValueControl.Text = "11";
                    break;
                case 17:
                    aControl.Text = "2197";
                    cControl.Text = "1597";
                    mControl.Text = "67108863";
                    startValueControl.Text = "13";
                    break;
                case 18:
                    aControl.Text = "2744";
                    cControl.Text = "2584";
                    mControl.Text = "134217727";
                    startValueControl.Text = "17";
                    break;
                case 19:
                    aControl.Text = "3375";
                    cControl.Text = "4181";
                    mControl.Text = "268435455";
                    startValueControl.Text = "19";
                    break;
                case 20:
                    aControl.Text = "4096";
                    cControl.Text = "6765";
                    mControl.Text = "536870911";
                    startValueControl.Text = "23";
                    break;
                case 21:
                    aControl.Text = "4913";
                    cControl.Text = "10946";
                    mControl.Text = "1073741823";
                    startValueControl.Text = "29";
                    break;
                case 22:
                    aControl.Text = "16807";
                    cControl.Text = "17711";
                    mControl.Text = "2147483647";
                    startValueControl.Text = "31";
                    break;
                case 23:
                    aControl.Text = "65536";
                    cControl.Text = "28657";
                    mControl.Text = "2147483648";
                    startValueControl.Text = "33";
                    break;
                case 24:
                    aControl.Text = "32768";
                    cControl.Text = "46368";
                    mControl.Text = "2147483645";
                    startValueControl.Text = "37";
                    break;
                case 25:
                    aControl.Text = "16384";
                    cControl.Text = "75025";
                    mControl.Text = "2147483641";
                    startValueControl.Text = "41";
                    break;
                default:
                    aControl.Text = "1";
                    cControl.Text = "1";
                    mControl.Text = "1";
                    startValueControl.Text = "1";
                    break;
            }
        }
    }
}
