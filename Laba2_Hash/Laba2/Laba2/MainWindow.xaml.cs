﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Laba2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool isFileHashed = false;
        private bool isStringHashed = false;
        private bool isFileHashLoaded = false;

        private MyMD5HashClass myStringHashMaker;
        private MyMD5HashClass myFileHashMaker;

        public MainWindow()
        {
            InitializeComponent();
            myStringHashMaker = new MyMD5HashClass();
            myFileHashMaker = new MyMD5HashClass();
        }

        private void hashStringButton_Click(object sender, RoutedEventArgs e)
        {
            myStringHashMaker.createHash(hashTextBox.Text);
            hashLabel.Content = myStringHashMaker.stringRezultForHash;
            isStringHashed = true;
        }

        private async void hashFileButton_Click(object sender, RoutedEventArgs e)
        {
            string filePath = null;

            OpenFileDialog hashFileDialog = new OpenFileDialog();
            hashFileDialog.Filter = "Text|*.txt|All|*.*";
            hashFileDialog.FilterIndex = 1;
            hashFileDialog.Multiselect = false;

            if (hashFileDialog.ShowDialog() == true)
            {
                filePath = hashFileDialog.FileName;
            }

            using (var fileReadingStream = File.OpenRead(filePath))
            {
                var varForHashResult = await myFileHashMaker.ComputeFileHashAsync(filePath);
                hashFileLabel.Content = BitConverter.ToString(varForHashResult.convertWordsToByteArray()).Replace("-", "").ToLowerInvariant();
            }

            isFileHashed = true;
        }

        private void saveHashInFileButton_Click(object sender, RoutedEventArgs e)
        {
            if (saveFromStringRadioButton.IsChecked == true || saveFromFileRadioButton.IsChecked == true)
            {
                if ( (isFileHashed && saveFromFileRadioButton.IsChecked == true) || (isStringHashed && saveFromStringRadioButton.IsChecked == true))
                {
                    string filePath = null;

                    SaveFileDialog saveHashInFileDialog = new SaveFileDialog();
                    saveHashInFileDialog.Filter = "Text|*.txt|All|*.*";
                    saveHashInFileDialog.FilterIndex = 1;

                    if (saveHashInFileDialog.ShowDialog() == true)
                    {
                        filePath = saveHashInFileDialog.FileName;
                    }

                    File.WriteAllText(filePath, (string)hashLabel.Content);
                    saveHashFileLabel.Content = filePath;
                }
                else
                {
                    MessageBox.Show("Nothing to save. Hash string/file at first.");
                }
            }
            else
            {
                MessageBox.Show("Select source for hash saving (string/file).");
            }
        }

        private void loadHashFromFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog loadHashFromFileDialog = new OpenFileDialog();
            loadHashFromFileDialog.Filter = "Text|*.txt";
            loadHashFromFileDialog.FilterIndex = 1;
            loadHashFromFileDialog.Multiselect = false;
            string filePath = null;

            if (loadHashFromFileDialog.ShowDialog() == true)
            {
                filePath = loadHashFromFileDialog.FileName;
            }

            string readText = File.ReadAllText(filePath);
            loadedHashLabel.Content = readText;
            isFileHashLoaded = true;
            invisiblePathLabel.Content = filePath;
            invisiblePathLabel.Visibility = Visibility.Visible;
            invisibleLabel.Visibility = Visibility.Visible;
        }

        private void checkIntegrityButton_Click(object sender, RoutedEventArgs e)
        {
            if (isFileHashed)
            {
                if (isFileHashLoaded)
                {
                    if (loadedHashLabel.Content.ToString() == hashFileLabel.Content.ToString())
                    {
                        MessageBox.Show("Hashes are equal, file was integral.");
                    }
                    else
                    {
                        MessageBox.Show("Hashed file checksum and loaded hash checksum aren`t equal. Maybe you choosed wrong file.");
                    }
                }
                else
                {
                    MessageBox.Show("Load hash from file at first.");
                }
            }
            else
            {
                MessageBox.Show("There was no hashed file.");
            }
        }

        /// <summary>
        /// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>

        private class MyMD5HashClass
        {
            public string stringRezultForHash => instanceForHash.ToString();
            public roundMakerClass instanceForHash { get; private set; }
            private const int numberForOptimalMultiplication = 100_000;
            private const UInt32 preferredSizeForBufferUnit = constantsForAlgorithm.numberOfBytesIn512BitSizedBlock * numberForOptimalMultiplication;

            public void createHash(string input)
            {
                createHash(Encoding.ASCII.GetBytes(input));
            }

            public roundMakerClass createHash(Byte[] input)
            {
                instanceForHash = roundMakerClass.defaultStartValue;

                var paddedMessage = calculateUnitedArray(input, calculateMessageIndentation((UInt32)input.Length));

                for (UInt32 bNo = 0; bNo < paddedMessage.Length / constantsForAlgorithm.numberOfBytesIn512BitSizedBlock; ++bNo)
                {
                    UInt32[] X = assistantClassForBitsOperations.get32BitWords(
                        paddedMessage,
                        bNo,
                        constantsForAlgorithm.numberOfWordsIn32BitSizedArray * constantsForAlgorithm.numberOfBytesIn32BitWord);

                    FeedMessageBlockToBeHashed(X);
                }

                return instanceForHash;
            }

            public async Task<roundMakerClass> ComputeFileHashAsync(String filePath)
            {
                instanceForHash = roundMakerClass.defaultStartValue;
                using (var fileStream = File.OpenRead(filePath))
                {
                    UInt64 numberOfBytesReaded = 0;
                    Int32 numberOfCurrentBytesReaded = 0;
                    bool isFileEnded = false;
                    do
                    {
                        var hashUnit = new Byte[preferredSizeForBufferUnit];

                        numberOfCurrentBytesReaded = await fileStream.ReadAsync(hashUnit, 0, hashUnit.Length);
                        numberOfBytesReaded += (UInt64)numberOfCurrentBytesReaded;

                        if (numberOfCurrentBytesReaded < hashUnit.Length)
                        {
                            Byte[] lastUnit;

                            if (numberOfCurrentBytesReaded == 0)
                            {
                                lastUnit = calculateMessageIndentation(numberOfBytesReaded);
                            }
                            else
                            {
                                lastUnit = new Byte[numberOfCurrentBytesReaded];
                                Array.Copy(hashUnit, lastUnit, numberOfCurrentBytesReaded);

                                lastUnit = calculateUnitedArray(lastUnit, calculateMessageIndentation(numberOfBytesReaded));
                            }

                            hashUnit = lastUnit;
                            isFileEnded = true;
                        }

                        for (UInt32 bNo = 0; bNo < hashUnit.Length / constantsForAlgorithm.numberOfBytesIn512BitSizedBlock; ++bNo)
                        {
                            UInt32[] X = assistantClassForBitsOperations.get32BitWords(
                                hashUnit,
                                bNo,
                                constantsForAlgorithm.numberOfWordsIn32BitSizedArray * constantsForAlgorithm.numberOfBytesIn32BitWord);

                            FeedMessageBlockToBeHashed(X);
                        }
                    }
                    while (isFileEnded == false);
                }

                return instanceForHash;
            }

            private void FeedMessageBlockToBeHashed(UInt32[] X)
            {
                UInt32 F, i, k;
                var blockSize = constantsForAlgorithm.numberOfBytesIn512BitSizedBlock;
                var MDq = instanceForHash.cloneRoundMaker();

                // first round
                for (i = 0; i < blockSize / 4; ++i)
                {
                    k = i;
                    F = assistantClassForBitsOperations.FuncF(MDq.BWord, MDq.CWord, MDq.DWord);

                    MDq.swapIterationForHashAlgorithm(F, X, i, k);
                }
                // second round
                for (; i < blockSize / 2; ++i)
                {
                    k = (1 + (5 * i)) % (blockSize / 4);
                    F = assistantClassForBitsOperations.FuncG(MDq.BWord, MDq.CWord, MDq.DWord);

                    MDq.swapIterationForHashAlgorithm(F, X, i, k);
                }
                // third round
                for (; i < blockSize / 4 * 3; ++i)
                {
                    k = (5 + (3 * i)) % (blockSize / 4);
                    F = assistantClassForBitsOperations.FuncH(MDq.BWord, MDq.CWord, MDq.DWord);

                    MDq.swapIterationForHashAlgorithm(F, X, i, k);
                }
                // fourth round
                for (; i < blockSize; ++i)
                {
                    k = 7 * i % (blockSize / 4);
                    F = assistantClassForBitsOperations.FuncI(MDq.BWord, MDq.CWord, MDq.DWord);

                    MDq.swapIterationForHashAlgorithm(F, X, i, k);
                }

                instanceForHash += MDq;
            }

            private static Byte[] calculateMessageIndentation(UInt64 messageLength)
            {
                UInt32 paddingLengthInBytes = default;
                var mod = (UInt32)(messageLength * constantsForAlgorithm.numberOfBitsInByte % constantsForAlgorithm.numberOfBitsIn512BitSizedBlock);

                // Append Padding Bits
                if (mod == constantsForAlgorithm.constantFor448Bits)
                {
                    paddingLengthInBytes = constantsForAlgorithm.numberOfBitsIn512BitSizedBlock / constantsForAlgorithm.numberOfBitsInByte;
                }
                else if (mod > constantsForAlgorithm.constantFor448Bits)
                {
                    paddingLengthInBytes = (constantsForAlgorithm.numberOfBitsIn512BitSizedBlock - mod + constantsForAlgorithm.constantFor448Bits) / constantsForAlgorithm.numberOfBitsInByte;
                }
                else if (mod < constantsForAlgorithm.constantFor448Bits)
                {
                    paddingLengthInBytes = (constantsForAlgorithm.constantFor448Bits - mod) / constantsForAlgorithm.numberOfBitsInByte;
                }

                var padding = new Byte[paddingLengthInBytes + constantsForAlgorithm.numberOfBitsInByte];
                padding[0] = constantsForAlgorithm.constantFor128Bits;

                // Append Length
                var messageLength64bit = messageLength * constantsForAlgorithm.numberOfBitsInByte;

                for (var i = 0; i < constantsForAlgorithm.numberOfBitsInByte; ++i)
                {
                    padding[paddingLengthInBytes + i] = (Byte)(messageLength64bit
                        >> (Int32)(i * constantsForAlgorithm.numberOfBitsInByte)
                        & constantsForAlgorithm.constantFor255Bits);
                }

                return padding;
            }

            private static T[] calculateUnitedArray<T>(T[] firstArray, T[] secondArray)
            {
                T[] unitedArray = new T[firstArray.Length + secondArray.Length];
                Array.Copy(firstArray, unitedArray, firstArray.Length);
                Array.Copy(secondArray, 0, unitedArray, firstArray.Length, secondArray.Length);

                return unitedArray;
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public class roundMakerClass
        {
            internal static roundMakerClass defaultStartValue { get; }

            static roundMakerClass()
            {
                defaultStartValue = new roundMakerClass
                {
                    AWord = constantsForAlgorithm.startValueInWordAForBuffer,
                    BWord = constantsForAlgorithm.startValueInWordBForBuffer,
                    CWord = constantsForAlgorithm.startValueInWordCForBuffer,
                    DWord = constantsForAlgorithm.startValueInWordDForBuffer
                };
            }
            public UInt32 AWord { get; set; }
            public UInt32 BWord { get; set; }
            public UInt32 CWord { get; set; }
            public UInt32 DWord { get; set; }
            public Byte[] convertWordsToByteArray()
            {
                return assistantClassForArrays.concatinateArrays(
                    BitConverter.GetBytes(AWord),
                    BitConverter.GetBytes(BWord),
                    BitConverter.GetBytes(CWord),
                    BitConverter.GetBytes(DWord));
            }

            public roundMakerClass cloneRoundMaker()
            {
                return MemberwiseClone() as roundMakerClass;
            }

            internal void swapIterationForHashAlgorithm(UInt32 FValue, UInt32[] XValue, UInt32 iValue, UInt32 kValue)
            {
                var temporaryDWord = DWord;
                DWord = CWord;
                CWord = BWord;
                BWord += assistantClassForBitsOperations.LeftRotate(AWord + FValue + XValue[kValue] + constantsForAlgorithm.sinusValuesTableArray[iValue], constantsForAlgorithm.eachCycleCyclicShiftSValuesTable[iValue]);
                AWord = temporaryDWord;
            }

            public override String ToString()
            {
                return $"{ToByteString(AWord)}{ToByteString(BWord)}{ToByteString(CWord)}{ToByteString(DWord)}";
            }

            public override Int32 GetHashCode()
            {
                return ToString().GetHashCode();
            }

            public override Boolean Equals(Object value)
            {
                return value is roundMakerClass md
                    && (GetHashCode() == md.GetHashCode() || ToString() == md.ToString());
            }

            public static roundMakerClass operator +(roundMakerClass left, roundMakerClass right)
            {
                return new roundMakerClass
                {
                    AWord = left.AWord + right.AWord,
                    BWord = left.BWord + right.BWord,
                    CWord = left.CWord + right.CWord,
                    DWord = left.DWord + right.DWord
                };
            }

            private static string ToByteString(UInt32 x)
            {
                return string.Join(string.Empty, BitConverter.GetBytes(x).Select(y => y.ToString("x2")));
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        internal static class constantsForAlgorithm
        {
            public const UInt32 numberOfBitsInByte = 8;
            public const UInt32 numberOfBytesIn32BitWord = 4;
            public const UInt32 numberOfWordsIn32BitSizedArray = 16;
            public const UInt32 numberOfBitsIn512BitSizedBlock = 512u;
            public const UInt32 numberOfBytesIn512BitSizedBlock = numberOfBitsIn512BitSizedBlock / numberOfBitsInByte;
            public const UInt32 constantFor448Bits = 448u;
            public const Byte constantFor255Bits = 0b11111111;
            public const Byte constantFor128Bits = 0b10000000;
            // Start value in word A of MD buffer
            public const UInt32 startValueInWordAForBuffer = 0x67452301;
            // Start value in word B of MD buffer
            public const UInt32 startValueInWordBForBuffer = 0xefcdab89;
            // Start value in word C of MD buffer
            public const UInt32 startValueInWordCForBuffer = 0x98badcfe;
            // Start value in word D of MD buffer
            public const UInt32 startValueInWordDForBuffer = 0x10325476;
            //sinus values table array
            public readonly static UInt32[] sinusValuesTableArray = new UInt32[64]
            {
            0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
            0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
            0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
            0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
            0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
            0xd62f105d, 0x2441453,  0xd8a1e681, 0xe7d3fbc8,
            0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
            0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
            0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
            0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
            0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x4881d05,
            0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
            0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
            0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
            0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
            0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
            };
            // Lookup table with round shift values
            public readonly static Int32[] eachCycleCyclicShiftSValuesTable = new Int32[] {
            7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
            5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
            4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
            6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21
            };
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        internal static class assistantClassForBitsOperations
        {

            public static UInt32[] get32BitWords(Byte[] message, UInt32 numberOfBlock, UInt32 blockSizeInBytes)
            {
                var messageStartIndex = numberOfBlock * blockSizeInBytes;
                var resultArray = new UInt32[blockSizeInBytes / constantsForAlgorithm.numberOfBytesIn32BitWord];

                for (UInt32 i = 0; i < blockSizeInBytes; i += constantsForAlgorithm.numberOfBytesIn32BitWord)
                {
                    var j = messageStartIndex + i;

                    resultArray[i / constantsForAlgorithm.numberOfBytesIn32BitWord] = // form 32-bit word from four bytes
                          message[j]                                                   // first byte
                        | (((UInt32)message[j + 1]) << ((Int32)constantsForAlgorithm.numberOfBitsInByte * 1))  // second byte
                        | (((UInt32)message[j + 2]) << ((Int32)constantsForAlgorithm.numberOfBitsInByte * 2))  // third byte
                        | (((UInt32)message[j + 3]) << ((Int32)constantsForAlgorithm.numberOfBitsInByte * 3)); // fourth byte
                }

                return resultArray;
            }

            public static UInt32 LeftRotate(UInt32 value, Int32 shiftValue)
            {
                return (value << shiftValue)
                     | (value >> (Int32)(constantsForAlgorithm.numberOfBitsInByte * constantsForAlgorithm.numberOfBytesIn32BitWord - shiftValue));
            }

            public static UInt32 FuncF(UInt32 B, UInt32 C, UInt32 D) => (B & C) | (~B & D);
            public static UInt32 FuncG(UInt32 B, UInt32 C, UInt32 D) => (D & B) | (C & ~D);
            public static UInt32 FuncH(UInt32 B, UInt32 C, UInt32 D) => B ^ C ^ D;
            public static UInt32 FuncI(UInt32 B, UInt32 C, UInt32 D) => C ^ (B | ~D);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static class assistantClassForArrays
        {
            public static T[] concatinateArrays<T>(params T[][] arrays)
            {
                var arrayIndex = 0;
                var resultArray = new T[arrays.Sum(a => a.Length)];

                foreach (var item in arrays)
                {
                    Array.Copy(item, 0, resultArray, arrayIndex, item.Length);
                    arrayIndex += item.Length;
                }

                return resultArray;
            }
        }
    }
}
