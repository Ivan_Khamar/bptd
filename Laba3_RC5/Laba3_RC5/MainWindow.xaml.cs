﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static Laba3_RC5.MainWindow;

namespace Laba3_RC5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public enum numberOfBytesInKeyEnum
        {
            length8Bytes = 8,
            length16Bytes = 16,
            length32Bytes = 32
        }
        private readonly cypherAlgorithmRC5Class cypherAlgorithmRC5Instance;
        private string stringFilePath;
        private const numberOfBytesInKeyEnum numberOfBytesInKey = numberOfBytesInKeyEnum.length8Bytes;

        public MainWindow()
        {
            InitializeComponent();
            cypherAlgorithmRC5Instance = new cypherAlgorithmRC5Class(new cypherAlgorithmRC5Parameters
            {
                numberOfRounds = numberOfRoundsInAlgorithmEnum.numberOfRoundsInAlgorithm8,
                numberOfBitsInWord = numberOfBitsInWordEnum.numberOfBitsInWord16
            });
        }

        private void chooseFileButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialogInstance = new OpenFileDialog();
            openFileDialogInstance.Filter = "Text|*.txt|All|*.*";
            openFileDialogInstance.FilterIndex = 1;
            openFileDialogInstance.Multiselect = false;
            string filePath = null;
            string[] filesPathsArray = null;

            if (openFileDialogInstance.ShowDialog() == true)
            {
                filePath = openFileDialogInstance.FileName;
                filesPathsArray = openFileDialogInstance.FileNames;
            }

            string textInFile = File.ReadAllText(filePath);
            fileTextBox.Text = filePath;
            stringFilePath = filePath;
        }

        private void encryptFileButton_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(stringFilePath))
            {
                MessageBox.Show("File not chosen!", "RC5");
                return;
            }

            try
            {
                var MD5hashOfPass = Encoding.UTF8.GetBytes(passTextBox.Text).methodForGettingMD5HashedKeyForRC5(numberOfBytesInKey);
                var encodedFileContent = cypherAlgorithmRC5Instance.EncipherCBCPAD(File.ReadAllBytes(stringFilePath),MD5hashOfPass);

                File.WriteAllBytes(PaddFilename(stringFilePath, "-enc"), encodedFileContent);

                MessageBox.Show("Enciphered", "RC5");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "RC5");
            }
        }

        private void decryptFileButton_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(stringFilePath))
            {
                MessageBox.Show("File not choosen!", "RC5");
                return;
            }
            try
            {
                var MD5hashOfPass = Encoding.UTF8.GetBytes(passTextBox.Text).methodForGettingMD5HashedKeyForRC5(numberOfBytesInKey);
                var decodedFileContent = cypherAlgorithmRC5Instance.DecipherCBCPAD(File.ReadAllBytes(stringFilePath),MD5hashOfPass);

                File.WriteAllBytes(PaddFilename(stringFilePath, "-dec"), decodedFileContent);

                MessageBox.Show("Deciphered", "RC5");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "RC5");
            }
        }

        private static String PaddFilename(string filePath, string padding)
        {
            var informationAboutFile = new FileInfo(filePath);
            var nameOfAFile = System.IO.Path.GetFileNameWithoutExtension(filePath);

            return System.IO.Path.Combine(informationAboutFile.DirectoryName,nameOfAFile + padding + informationAboutFile.Extension);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class cypherAlgorithmRC5Class
    {
        private readonly IPseudoRandomNumberGenerator pseudoRandomNumberGeneratorInstance;
        private readonly IWordFactory wordFactoryInstance;
        private readonly Int32 numberOfRounds;

        public cypherAlgorithmRC5Class(cypherAlgorithmRC5Parameters cypherParametersInstance)
        {
            pseudoRandomNumberGeneratorInstance = new PseudoRandomNumberGenerator(PseudoRandomNumberGeneratorOptions.Optimal);

            wordFactoryInstance = cypherParametersInstance.numberOfBitsInWord.GetWordFactory();
            numberOfRounds = (int)cypherParametersInstance.numberOfRounds;
        }

        public Byte[] EncipherCBCPAD(Byte[] input, Byte[] key)
        {
            var paddedBytes = concatinationArraysClass.methodForArraysConcatination(input, GetPadding(input));
            var bytesPerBlock = wordFactoryInstance.numberOfBytesInBlock;
            var s = createExpandedKeyTable(key);
            var cnPrev = GetRandomBytesForInitVector().Take(bytesPerBlock).ToArray();
            var encodedFileContent = new Byte[cnPrev.Length + paddedBytes.Length];

            EncipherECB(cnPrev, encodedFileContent, inStart: 0, outStart: 0, s);

            for (int i = 0; i < paddedBytes.Length; i += bytesPerBlock)
            {
                var cn = new Byte[bytesPerBlock];
                Array.Copy(paddedBytes, i, cn, 0, cn.Length);

                cn.XorWith(
                    xorArray: cnPrev,
                    inStartIndex: 0,
                    xorStartIndex: 0,
                    length: cn.Length);

                EncipherECB(
                    inBytes: cn,
                    outBytes: encodedFileContent,
                    inStart: 0,
                    outStart: i + bytesPerBlock,
                    s: s);

                Array.Copy(encodedFileContent, i + bytesPerBlock, cnPrev, 0, cn.Length);
            }

            return encodedFileContent;
        }

        public Byte[] DecipherCBCPAD(Byte[] input, Byte[] key)
        {
            var numberOfBytesInBlock = wordFactoryInstance.numberOfBytesInBlock;
            var s = createExpandedKeyTable(key);
            var previousCN = new Byte[numberOfBytesInBlock];
            var decodedFileContent = new Byte[input.Length - previousCN.Length];

            DecipherECB(inBuf: input, outBuf: previousCN, inStart: 0, outStart: 0, s: s);

            for (int i = numberOfBytesInBlock; i < input.Length; i += numberOfBytesInBlock)
            {
                var cn = new Byte[numberOfBytesInBlock];
                Array.Copy(input, i, cn, 0, cn.Length);

                DecipherECB(inBuf: cn, outBuf: decodedFileContent, inStart: 0, outStart: i - numberOfBytesInBlock, s: s);
                decodedFileContent.XorWith(xorArray: previousCN, inStartIndex: i - numberOfBytesInBlock, xorStartIndex: 0, length: cn.Length);

                Array.Copy(input, i, previousCN, 0, previousCN.Length);
            }

            var decodedWithoutPadding = new Byte[decodedFileContent.Length - decodedFileContent.Last()];
            Array.Copy(decodedFileContent, decodedWithoutPadding, decodedWithoutPadding.Length);

            return decodedWithoutPadding;
        }

        private void EncipherECB(Byte[] inBytes, Byte[] outBytes, Int32 inStart, Int32 outStart, IWord[] s)
        {
            var a = wordFactoryInstance.CreateFromBytes(inBytes, inStart);
            var b = wordFactoryInstance.CreateFromBytes(inBytes, inStart + wordFactoryInstance.numberOfBytesInWord);

            a.Add(s[0]);
            b.Add(s[1]);

            for (var i = 1; i < numberOfRounds + 1; ++i)
            {
                a.XorWith(b).ROL(b.ToInt32()).Add(s[2 * i]);
                b.XorWith(a).ROL(a.ToInt32()).Add(s[2 * i + 1]);
            }

            a.FillBytesArray(outBytes, outStart);
            b.FillBytesArray(outBytes, outStart + wordFactoryInstance.numberOfBytesInWord);
        }

        private void DecipherECB(Byte[] inBuf, Byte[] outBuf, Int32 inStart, Int32 outStart, IWord[] s)
        {
            var a = wordFactoryInstance.CreateFromBytes(inBuf, inStart);
            var b = wordFactoryInstance.CreateFromBytes(inBuf, inStart + wordFactoryInstance.numberOfBytesInWord);

            for (var i = numberOfRounds; i > 0; --i)
            {
                b = b.Sub(s[2 * i + 1]).ROR(a.ToInt32()).XorWith(a);
                a = a.Sub(s[2 * i]).ROR(b.ToInt32()).XorWith(b);
            }

            a.Sub(s[0]);
            b.Sub(s[1]);

            a.FillBytesArray(outBuf, outStart);
            b.FillBytesArray(outBuf, outStart + wordFactoryInstance.numberOfBytesInWord);
        }

        private Byte[] GetPadding(Byte[] inBytes)
        {
            var paddingLength = wordFactoryInstance.numberOfBytesInBlock - inBytes.Length % (wordFactoryInstance.numberOfBytesInBlock);
            var padding = new Byte[paddingLength];

            for (int i = 0; i < padding.Length; ++i)
            {
                padding[i] = (Byte)paddingLength;
            }

            return padding;
        }

        private Byte[] GetRandomBytesForInitVector()
        {
            var ivParts = new List<Byte[]>();

            while (ivParts.Sum(ivp => ivp.Length) < wordFactoryInstance.numberOfBytesInBlock)
            {
                ivParts.Add(BitConverter.GetBytes(pseudoRandomNumberGeneratorInstance.NextNumber()));
            }

            return concatinationArraysClass.methodForArraysConcatination(ivParts.ToArray());
        }

        private IWord[] createExpandedKeyTable(Byte[] key)
        {
            var keysWordArrLength = key.Length % wordFactoryInstance.numberOfBytesInWord > 0
                ? key.Length / wordFactoryInstance.numberOfBytesInWord + 1
                : key.Length / wordFactoryInstance.numberOfBytesInWord;

            var lArr = new IWord[keysWordArrLength];

            for (int i = 0; i < lArr.Length; i++)
            {
                lArr[i] = wordFactoryInstance.Create();
            }

            for (var i = key.Length - 1; i >= 0; i--)
            {
                lArr[i / wordFactoryInstance.numberOfBytesInWord].ROL(constantsForRC5.numberOfBitsInByte).Add(key[i]);
            }

            var sArray = new IWord[2 * (numberOfRounds + 1)];
            sArray[0] = wordFactoryInstance.CreateP();
            var q = wordFactoryInstance.CreateQ();

            for (var i = 1; i < sArray.Length; i++)
            {
                sArray[i] = sArray[i - 1].Clone();
                sArray[i].Add(q);
            }

            var x = wordFactoryInstance.Create();
            var y = wordFactoryInstance.Create();

            var n = 3 * Math.Max(sArray.Length, lArr.Length);

            for (Int32 k = 0, i = 0, j = 0; k < n; ++k)
            {
                sArray[i].Add(x).Add(y).ROL(3);
                x = sArray[i].Clone();

                lArr[j].Add(x).Add(y).ROL(x.ToInt32() + y.ToInt32());
                y = lArr[j].Clone();

                i = (i + 1) % sArray.Length;
                j = (j + 1) % lArr.Length;
            }

            return sArray;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////

    public static class concatinationArraysClass
    {
        public static T[] methodForArraysConcatination<T>(params T[][] arrays)
        {
            var position = 0;
            var outputArray = new T[arrays.Sum(a => a.Length)];

            foreach (var a in arrays)
            {
                Array.Copy(a, 0, outputArray, position, a.Length);
                position += a.Length;
            }

            return outputArray;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    internal interface IWord
    {
        void CreateFromBytes(byte[] bytes, Int32 startFromIndex);
        Byte[] FillBytesArray(Byte[] bytesToFill, Int32 startFromIndex);
        IWord ROL(Int32 offset);
        IWord ROR(Int32 offset);
        IWord XorWith(IWord word);
        IWord Add(IWord word);
        IWord Add(byte value);
        IWord Sub(IWord word);
        IWord Clone();
        Int32 ToInt32();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static class constantsForRC5
    {
        public const UInt16 P16 = 0xB7E1;
        public const UInt16 Q16 = 0x9E37;
        public const UInt32 P32 = 0xB7E15162;
        public const UInt32 Q32 = 0x9E3779B9;
        public const UInt64 P64 = 0xB7E151628AED2A6B;
        public const UInt64 Q64 = 0x9E3779B97F4A7C15;

        public const Int32 numberOfBitsInByte = 8;
        public const Int32 ByteMask = 0b11111111;

        public const Int32 MinRoundCount = 0;
        public const Int32 MaxRoundCount = 255;
        public const Int32 MinSecretKeyOctetsCount = 0;
        public const Int32 MaxSecretKeyOctetsCount = 255;
        public const Int32 MinKeySizeInBytes = 0;
        public const Int32 MaxKeySizeInBytes = 2040;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    public interface IPseudoRandomNumberGenerator
    {
        ulong NextNumber();
        ulong NextNumber(ulong currentNumber);
        void Reset();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    internal interface IWordFactory
    {
        Int32 numberOfBytesInWord { get; }
        Int32 numberOfBytesInBlock { get; }
        IWord CreateQ();
        IWord CreateP();
        IWord Create();
        IWord CreateFromBytes(byte[] bytes, Int32 startFrom);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class cypherAlgorithmRC5Parameters
    {
        public numberOfRoundsInAlgorithmEnum numberOfRounds { get; set; }
        public numberOfBitsInWordEnum numberOfBitsInWord { get; set; }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public enum numberOfRoundsInAlgorithmEnum
    {
        numberOfRoundsInAlgorithm8 = 8,
        numberOfRoundsInAlgorithm12 = 12,
        numberOfRoundsInAlgorithm16 = 16,
        numberOfRoundsInAlgorithm20 = 20
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public enum numberOfBitsInWordEnum
    {
        numberOfBitsInWord16,
        numberOfBitsInWord32,
        numberOfBitsInWord64
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class PseudoRandomNumberGenerator : IPseudoRandomNumberGenerator
    {
        private ulong _currentNumber;
        private readonly ulong _mod;
        private readonly ulong _multiplier;
        private readonly ulong _cummulative;
        private readonly ulong _startValue;

        public PseudoRandomNumberGenerator(PseudoRandomNumberGeneratorOptions options)
        {
            _mod = options.Mod;
            _multiplier = options.Multiplier;
            _cummulative = options.Cummulative;
            _startValue = options.StartValue;
            _currentNumber = options.StartValue;
        }

        public ulong NextNumber()
        {
            var nextNumber = _currentNumber;

            _currentNumber = SequenceFormula(
                _multiplier,
                _cummulative,
                _mod,
                nextNumber);

            return nextNumber;
        }

        public ulong NextNumber(ulong currentNumber)
        {
            return SequenceFormula(
                _multiplier,
                _cummulative,
                _mod,
                currentNumber);
        }

        public void Reset()
        {
            _currentNumber = _startValue;
        }

        private static ulong SequenceFormula(
            ulong mult,
            ulong c,
            ulong mod,
            ulong x)
        {
            return (mult * x + c) % mod;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class PseudoRandomNumberGeneratorOptions
    {
        static PseudoRandomNumberGeneratorOptions()
        {
            Default = new PseudoRandomNumberGeneratorOptions
            {
                Mod = (ulong)Math.Pow(2, 26) - 1,
                Cummulative = 1597,
                Multiplier = (ulong)Math.Pow(13, 3),
                StartValue = 13
            };

            Optimal = new PseudoRandomNumberGeneratorOptions
            {
                Mod = (ulong)Math.Pow(2, 31) - 1,
                Cummulative = 17711,
                Multiplier = (ulong)Math.Pow(7, 5),
                StartValue = 31
            };
        }

        public static PseudoRandomNumberGeneratorOptions Default { get; }
        public static PseudoRandomNumberGeneratorOptions Optimal { get; }

        public ulong Mod { get; set; }

        public ulong Multiplier { get; set; }

        public ulong Cummulative { get; set; }

        public ulong StartValue { get; set; }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    internal static class WordLengthInBitsEnumExtensions
    {
        internal static IWordFactory GetWordFactory(this numberOfBitsInWordEnum wordLengthInBits)
        {
            IWordFactory wordFactory = null;

            switch (wordLengthInBits)
            {
                case numberOfBitsInWordEnum.numberOfBitsInWord16:
                    wordFactory = new Word16BitFactory();
                    break;
                case numberOfBitsInWordEnum.numberOfBitsInWord32:
                    wordFactory = new Word32BitFactory();
                    break;
                case numberOfBitsInWordEnum.numberOfBitsInWord64:
                    wordFactory = new Word64BitFactory();
                    break;
                default:
                    throw new ArgumentException($"Invalid {nameof(numberOfBitsInWordEnum)} value.");
            }

            return wordFactory;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    internal class Word16BitFactory : IWordFactory
    {
        public Int32 numberOfBytesInWord => Word16Bit.BytesPerWord;

        public Int32 numberOfBytesInBlock => numberOfBytesInWord * 2;

        public IWord Create()
        {
            return CreateConcrete();
        }

        public IWord CreateP()
        {
            return CreateConcrete(constantsForRC5.P16);
        }

        public IWord CreateQ()
        {
            return CreateConcrete(constantsForRC5.Q16);
        }

        public IWord CreateFromBytes(Byte[] bytes, Int32 startFromIndex)
        {
            var word = Create();
            word.CreateFromBytes(bytes, startFromIndex);

            return word;
        }

        private Word16Bit CreateConcrete(UInt16 value = 0)
        {
            return new Word16Bit
            {
                WordValue = value
            };
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    internal class Word16Bit : IWord
    {
        public const Int32 WordSizeInBits = BytesPerWord * constantsForRC5.numberOfBitsInByte;
        public const Int32 BytesPerWord = sizeof(UInt16);

        public UInt16 WordValue { get; set; }

        public void CreateFromBytes(Byte[] bytes, Int32 startFrom)
        {
            WordValue = 0;

            for (var i = startFrom + BytesPerWord - 1; i > startFrom; --i)
            {
                WordValue = (UInt16)(WordValue | bytes[i]);
                WordValue = (UInt16)(WordValue << constantsForRC5.numberOfBitsInByte);
            }

            WordValue = (UInt16)(WordValue | bytes[startFrom]);
        }

        public Byte[] FillBytesArray(Byte[] bytesToFill, Int32 startFrom)
        {
            var i = 0;
            for (; i < BytesPerWord - 1; ++i)
            {
                bytesToFill[startFrom + i] = (Byte)(WordValue & constantsForRC5.ByteMask);
                WordValue = (UInt16)(WordValue >> constantsForRC5.numberOfBitsInByte);
            }

            bytesToFill[startFrom + i] = (Byte)(WordValue & constantsForRC5.ByteMask);

            return bytesToFill;
        }

        public IWord ROL(Int32 offset)
        {
            offset %= BytesPerWord;
            WordValue = (UInt16)((WordValue << offset) | (WordValue >> (WordSizeInBits - offset)));

            return this;
        }

        public IWord ROR(Int32 offset)
        {
            offset %= BytesPerWord;
            WordValue = (UInt16)((WordValue >> offset) | (WordValue << (WordSizeInBits - offset)));

            return this;
        }

        public IWord Add(IWord word)
        {
            WordValue = (UInt16)(WordValue + (word as Word16Bit).WordValue);

            return this;
        }

        public IWord Add(Byte value)
        {
            WordValue = (UInt16)(WordValue + value);

            return this;
        }

        public IWord Sub(IWord word)
        {
            WordValue = (UInt16)(WordValue - (word as Word16Bit).WordValue);

            return this;
        }

        public IWord XorWith(IWord word)
        {
            WordValue = (UInt16)(WordValue ^ (word as Word16Bit).WordValue);

            return this;
        }

        public IWord Clone()
        {
            return (IWord)MemberwiseClone();
        }

        public Int32 ToInt32()
        {
            return WordValue;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    internal class Word32BitFactory : IWordFactory
    {
        public Int32 numberOfBytesInWord => Word32Bit.BytesPerWord;

        public Int32 numberOfBytesInBlock => numberOfBytesInWord * 2;

        public IWord Create()
        {
            return CreateConcrete();
        }

        public IWord CreateP()
        {
            return CreateConcrete(constantsForRC5.P32);
        }

        public IWord CreateQ()
        {
            return CreateConcrete(constantsForRC5.Q32);
        }

        public IWord CreateFromBytes(Byte[] bytes, Int32 startFromIndex)
        {
            var word = Create();
            word.CreateFromBytes(bytes, startFromIndex);

            return word;
        }

        public Word32Bit CreateConcrete(UInt32 value = 0)
        {
            return new Word32Bit
            {
                WordValue = value
            };
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    internal class Word32Bit : IWord
    {
        public const Int32 WordSizeInBits = BytesPerWord * constantsForRC5.numberOfBitsInByte;
        public const Int32 BytesPerWord = sizeof(UInt32);

        public UInt32 WordValue { get; set; }

        public void CreateFromBytes(Byte[] bytes, Int32 startFrom)
        {
            WordValue = 0;

            for (var i = startFrom + BytesPerWord - 1; i > startFrom; --i)
            {
                WordValue |= bytes[i];
                WordValue <<= constantsForRC5.numberOfBitsInByte;
            }

            WordValue |= bytes[startFrom];
        }

        public Byte[] FillBytesArray(Byte[] bytesToFill, Int32 startFrom)
        {
            var i = 0;
            for (; i < BytesPerWord - 1; ++i)
            {
                bytesToFill[startFrom + i] = (Byte)(WordValue & constantsForRC5.ByteMask);
                WordValue >>= constantsForRC5.numberOfBitsInByte;
            }

            bytesToFill[startFrom + i] = (Byte)(WordValue & constantsForRC5.ByteMask);

            return bytesToFill;
        }

        public IWord ROL(Int32 offset)
        {
            WordValue = (WordValue << offset) | (WordValue >> (WordSizeInBits - offset));

            return this;
        }

        public IWord ROR(Int32 offset)
        {
            WordValue = (WordValue >> offset) | (WordValue << (WordSizeInBits - offset));

            return this;
        }

        public IWord Add(IWord word)
        {
            WordValue += (word as Word32Bit).WordValue;

            return this;
        }

        public IWord Add(Byte value)
        {
            WordValue += value;

            return this;
        }

        public IWord Sub(IWord word)
        {
            WordValue -= (word as Word32Bit).WordValue;

            return this;
        }

        public IWord XorWith(IWord word)
        {
            WordValue ^= (word as Word32Bit).WordValue;

            return this;
        }

        public IWord Clone()
        {
            return (IWord)MemberwiseClone();
        }

        public Int32 ToInt32()
        {
            return (Int32)WordValue;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    internal class Word64BitFactory : IWordFactory
    {
        public Int32 numberOfBytesInWord => Word64Bit.BytesPerWord;

        public Int32 numberOfBytesInBlock => numberOfBytesInWord * 2;

        public IWord Create()
        {
            return CreateConcrete();
        }

        public IWord CreateP()
        {
            return CreateConcrete(constantsForRC5.P64);
        }

        public IWord CreateQ()
        {
            return CreateConcrete(constantsForRC5.Q64);
        }

        public IWord CreateFromBytes(Byte[] bytes, Int32 startFromIndex)
        {
            var word = Create();
            word.CreateFromBytes(bytes, startFromIndex);

            return word;
        }

        private Word64Bit CreateConcrete(UInt64 value = 0)
        {
            return new Word64Bit
            {
                WordValue = value
            };
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    internal class Word64Bit : IWord
    {
        public const Int32 WordSizeInBits = BytesPerWord * constantsForRC5.numberOfBitsInByte;
        public const Int32 BytesPerWord = sizeof(UInt64);

        public UInt64 WordValue { get; set; }

        public void CreateFromBytes(Byte[] bytes, Int32 startFrom)
        {
            WordValue = 0;

            for (var i = startFrom + BytesPerWord - 1; i > startFrom; --i)
            {
                WordValue |= bytes[i];
                WordValue <<= constantsForRC5.numberOfBitsInByte;
            }

            WordValue |= bytes[startFrom];
        }

        public Byte[] FillBytesArray(Byte[] bytesToFill, Int32 startFrom)
        {
            var i = 0;
            for (; i < BytesPerWord - 1; ++i)
            {
                bytesToFill[startFrom + i] = (Byte)(WordValue & constantsForRC5.ByteMask);
                WordValue >>= constantsForRC5.numberOfBitsInByte;
            }

            bytesToFill[startFrom + i] = (Byte)(WordValue & constantsForRC5.ByteMask);

            return bytesToFill;
        }

        public IWord ROL(Int32 offset)
        {
            WordValue = (WordValue << offset) | (WordValue >> (WordSizeInBits - offset));

            return this;
        }

        public IWord ROR(Int32 offset)
        {
            WordValue = (WordValue >> offset) | (WordValue << (WordSizeInBits - offset));

            return this;
        }

        public IWord Add(IWord word)
        {
            WordValue += (word as Word64Bit).WordValue;

            return this;
        }

        public IWord Add(Byte value)
        {
            WordValue += value;

            return this;
        }

        public IWord Sub(IWord word)
        {
            WordValue -= (word as Word64Bit).WordValue;

            return this;
        }

        public IWord XorWith(IWord word)
        {
            WordValue ^= (word as Word64Bit).WordValue;

            return this;
        }

        public IWord Clone()
        {
            return (IWord)MemberwiseClone();
        }

        public Int32 ToInt32()
        {
            return (Int32)WordValue;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static class operationsForArrayOfBytes
    {
        public static Byte[] methodForGettingMD5HashedKeyForRC5(
            this Byte[] key,
            numberOfBytesInKeyEnum keyLengthInBytes)
        {
            if (key is null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            var hasherInstance = new MyMD5HashClass();
            var bytesHash = hasherInstance.ComputeHash(key).ToByteArray();

            if (keyLengthInBytes == numberOfBytesInKeyEnum.length8Bytes)
            {
                bytesHash = bytesHash.Take(bytesHash.Length / 2).ToArray();
            }
            else if (keyLengthInBytes == numberOfBytesInKeyEnum.length32Bytes)
            {
                bytesHash = bytesHash
                    .Concat(hasherInstance.ComputeHash(bytesHash).ToByteArray())
                    .ToArray();
            }

            if (bytesHash.Length != (int)keyLengthInBytes)
            {
                throw new InvalidOperationException(
                    $"Internal error at {nameof(operationsForArrayOfBytes.methodForGettingMD5HashedKeyForRC5)} method, " +
                    $"hash result is not equal to {(int)keyLengthInBytes}.");
            }

            return bytesHash;
        }

        internal static void XorWith(
            this Byte[] array,
            Byte[] xorArray,
            int inStartIndex,
            int xorStartIndex,
            int length)
        {
            for (int i = 0; i < length; ++i)
            {
                array[i + inStartIndex] ^= xorArray[i + xorStartIndex];
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public sealed class MyMD5HashClass
    {
        private const int numberForOptimalMultiplication = 100_000;
        private const UInt32 preferredSizeForBufferUnit = constantsForAlgorithm.numberOfBytesIn512BitSizedBlock * numberForOptimalMultiplication;

        public string HashAsString => Hash.ToString();

        public MessageDigest Hash { get; private set; }

        public void ComputeHash(string message)
        {
            ComputeHash(Encoding.ASCII.GetBytes(message));
        }

        public MessageDigest ComputeHash(Byte[] message)
        {
            Hash = MessageDigest.InitialValue;
            var paddedMessage = JoinArrays(message, GetMessagePadding((UInt32)message.Length));

            for (UInt32 bNo = 0; bNo < paddedMessage.Length / constantsForAlgorithm.numberOfBytesIn512BitSizedBlock; ++bNo)
            {
                UInt32[] X = BitsHelper.Extract32BitWords(
                    paddedMessage,
                    bNo,
                    constantsForAlgorithm.numberOfWordsIn32BitSizedArray * constantsForAlgorithm.numberOfBytesIn32BitWord);

                FeedMessageBlockToBeHashed(X);
            }

            return Hash;
        }

        public async Task<MessageDigest> ComputeFileHashAsync(String filePath)
        {
            Hash = MessageDigest.InitialValue;

            using (var fs = File.OpenRead(filePath))
            {
                UInt64 totalBytesRead = 0;
                Int32 currentBytesRead = 0;
                bool isFileEnd = false;

                do
                {
                    var chunk = new Byte[preferredSizeForBufferUnit];

                    currentBytesRead = await fs.ReadAsync(chunk, 0, chunk.Length);
                    totalBytesRead += (UInt64)currentBytesRead;

                    if (currentBytesRead < chunk.Length)
                    {
                        Byte[] lastChunk;

                        if (currentBytesRead == 0)
                        {
                            lastChunk = GetMessagePadding(totalBytesRead);
                        }
                        else
                        {
                            lastChunk = new Byte[currentBytesRead];
                            Array.Copy(chunk, lastChunk, currentBytesRead);

                            lastChunk = JoinArrays(lastChunk, GetMessagePadding(totalBytesRead));
                        }

                        chunk = lastChunk;
                        isFileEnd = true;
                    }

                    for (UInt32 bNo = 0; bNo < chunk.Length / constantsForAlgorithm.numberOfBytesIn512BitSizedBlock; ++bNo)
                    {
                        UInt32[] X = BitsHelper.Extract32BitWords(
                            chunk,
                            bNo,
                            constantsForAlgorithm.numberOfWordsIn32BitSizedArray * constantsForAlgorithm.numberOfBytesIn32BitWord);

                        FeedMessageBlockToBeHashed(X);
                    }
                }
                while (isFileEnd == false);
            }
            return Hash;
        }

        private void FeedMessageBlockToBeHashed(UInt32[] X)
        {
            UInt32 F, i, k;
            var blockSize = constantsForAlgorithm.numberOfBytesIn512BitSizedBlock;
            var MDq = Hash.Clone();

            // first round
            for (i = 0; i < blockSize / 4; ++i)
            {
                k = i;
                F = BitsHelper.FuncF(MDq.B, MDq.C, MDq.D);

                MDq.MD5IterationSwap(F, X, i, k);
            }
            // second round
            for (; i < blockSize / 2; ++i)
            {
                k = (1 + (5 * i)) % (blockSize / 4);
                F = BitsHelper.FuncG(MDq.B, MDq.C, MDq.D);

                MDq.MD5IterationSwap(F, X, i, k);
            }
            // third round
            for (; i < blockSize / 4 * 3; ++i)
            {
                k = (5 + (3 * i)) % (blockSize / 4);
                F = BitsHelper.FuncH(MDq.B, MDq.C, MDq.D);

                MDq.MD5IterationSwap(F, X, i, k);
            }
            // fourth round
            for (; i < blockSize; ++i)
            {
                k = 7 * i % (blockSize / 4);
                F = BitsHelper.FuncI(MDq.B, MDq.C, MDq.D);

                MDq.MD5IterationSwap(F, X, i, k);
            }

            Hash += MDq;
        }

        private static Byte[] GetMessagePadding(UInt64 messageLength)
        {
            UInt32 paddingLengthInBytes = default;
            var mod = (UInt32)(messageLength * constantsForAlgorithm.numberOfBitsInByte % constantsForAlgorithm.numberOfBitsIn512BitSizedBlock);

            // Append Padding Bits
            if (mod == constantsForAlgorithm.constantFor448Bits)
            {
                paddingLengthInBytes = constantsForAlgorithm.numberOfBitsIn512BitSizedBlock / constantsForAlgorithm.numberOfBitsInByte;
            }
            else if (mod > constantsForAlgorithm.constantFor448Bits)
            {
                paddingLengthInBytes = (constantsForAlgorithm.numberOfBitsIn512BitSizedBlock - mod + constantsForAlgorithm.constantFor448Bits) / constantsForAlgorithm.numberOfBitsInByte;
            }
            else if (mod < constantsForAlgorithm.constantFor448Bits)
            {
                paddingLengthInBytes = (constantsForAlgorithm.constantFor448Bits - mod) / constantsForAlgorithm.numberOfBitsInByte;
            }

            var padding = new Byte[paddingLengthInBytes + constantsForAlgorithm.numberOfBitsInByte];
            padding[0] = constantsForAlgorithm.constantFor128Bits;

            // Append Length
            var messageLength64bit = messageLength * constantsForAlgorithm.numberOfBitsInByte;

            for (var i = 0; i < constantsForAlgorithm.numberOfBitsInByte; ++i)
            {
                padding[paddingLengthInBytes + i] = (Byte)(messageLength64bit
                    >> (Int32)(i * constantsForAlgorithm.numberOfBitsInByte)
                    & constantsForAlgorithm.constantFor255Bits);
            }

            return padding;
        }

        private static T[] JoinArrays<T>(T[] first, T[] second)
        {
            T[] joinedArray = new T[first.Length + second.Length];
            Array.Copy(first, joinedArray, first.Length);
            Array.Copy(second, 0, joinedArray, first.Length, second.Length);

            return joinedArray;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    internal static class constantsForAlgorithm
    {
        public const UInt32 numberOfBitsInByte = 8;
        public const UInt32 numberOfBytesIn32BitWord = 4;
        public const UInt32 numberOfWordsIn32BitSizedArray = 16;
        public const UInt32 numberOfBitsIn512BitSizedBlock = 512u;
        public const UInt32 numberOfBytesIn512BitSizedBlock = numberOfBitsIn512BitSizedBlock / numberOfBitsInByte;
        public const UInt32 constantFor448Bits = 448u;
        public const Byte constantFor255Bits = 0b11111111;
        public const Byte constantFor128Bits = 0b10000000;

        /// Initial value for A word of MD buffer
        public const UInt32 startValueInWordAForBuffer = 0x67452301;

        /// Initial value for B word of MD buffer
        public const UInt32 startValueInWordBForBuffer = 0xefcdab89;

        /// Initial value for C word of MD buffer
        public const UInt32 startValueInWordCForBuffer = 0x98badcfe;

        /// Initial value for D word of MD buffer
        public const UInt32 startValueInWordDForBuffer = 0x10325476;

        /// Lookup table with values based on sin() values
        public readonly static UInt32[] sinusValuesTableArray = new UInt32[64]
        {
            0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
            0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
            0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
            0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
            0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
            0xd62f105d, 0x2441453,  0xd8a1e681, 0xe7d3fbc8,
            0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
            0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
            0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
            0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
            0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x4881d05,
            0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
            0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
            0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
            0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
            0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
        };

        /// Lookup table with round shift values
        public readonly static Int32[] eachCycleCyclicShiftSValuesTable = new Int32[] {
            7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
            5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
            4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
            6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21
        };
    }

    //////////////////////////////////////////////////////////////////////////////////////////////

    internal static class BitsHelper
    {
        public static UInt32[] Extract32BitWords(Byte[] message, UInt32 blockNo, UInt32 blockSizeInBytes)
        {
            var messageStartIndex = blockNo * blockSizeInBytes;
            var extractedArray = new UInt32[blockSizeInBytes / constantsForAlgorithm.numberOfBytesIn32BitWord];

            for (UInt32 i = 0; i < blockSizeInBytes; i += constantsForAlgorithm.numberOfBytesIn32BitWord)
            {
                var j = messageStartIndex + i;

                extractedArray[i / constantsForAlgorithm.numberOfBytesIn32BitWord] = // form 32-bit word from four bytes
                      message[j]                                                   // first byte
                    | (((UInt32)message[j + 1]) << ((Int32)constantsForAlgorithm.numberOfBitsInByte * 1))  // second byte
                    | (((UInt32)message[j + 2]) << ((Int32)constantsForAlgorithm.numberOfBitsInByte * 2))  // third byte
                    | (((UInt32)message[j + 3]) << ((Int32)constantsForAlgorithm.numberOfBitsInByte * 3)); // fourth byte
            }

            return extractedArray;
        }

        public static UInt32 LeftRotate(UInt32 value, Int32 shiftValue)
        {
            return (value << shiftValue)
                 | (value >> (Int32)(constantsForAlgorithm.numberOfBitsInByte * constantsForAlgorithm.numberOfBytesIn32BitWord - shiftValue));
        }

        public static UInt32 FuncF(UInt32 B, UInt32 C, UInt32 D) => (B & C) | (~B & D);

        public static UInt32 FuncG(UInt32 B, UInt32 C, UInt32 D) => (D & B) | (C & ~D);

        public static UInt32 FuncH(UInt32 B, UInt32 C, UInt32 D) => B ^ C ^ D;

        public static UInt32 FuncI(UInt32 B, UInt32 C, UInt32 D) => C ^ (B | ~D);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class MessageDigest
    {
        internal static MessageDigest InitialValue { get; }

        static MessageDigest()
        {
            InitialValue = new MessageDigest
            {
                A = constantsForAlgorithm.startValueInWordAForBuffer,
                B = constantsForAlgorithm.startValueInWordBForBuffer,
                C = constantsForAlgorithm.startValueInWordCForBuffer,
                D = constantsForAlgorithm.startValueInWordDForBuffer
            };
        }

        public UInt32 A { get; set; }

        public UInt32 B { get; set; }

        public UInt32 C { get; set; }

        public UInt32 D { get; set; }

        public Byte[] ToByteArray()
        {
            return concatinationArraysClass.methodForArraysConcatination(
                BitConverter.GetBytes(A),
                BitConverter.GetBytes(B),
                BitConverter.GetBytes(C),
                BitConverter.GetBytes(D));
        }

        public MessageDigest Clone()
        {
            return MemberwiseClone() as MessageDigest;
        }

        internal void MD5IterationSwap(UInt32 F, UInt32[] X, UInt32 i, UInt32 k)
        {
            var tempD = D;
            D = C;
            C = B;
            B += BitsHelper.LeftRotate(A + F + X[k] + constantsForAlgorithm.sinusValuesTableArray[i], constantsForAlgorithm.eachCycleCyclicShiftSValuesTable[i]);
            A = tempD;
        }

        public override String ToString()
        {
            return $"{ToByteString(A)}{ToByteString(B)}{ToByteString(C)}{ToByteString(D)}";
        }

        public override Int32 GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override Boolean Equals(Object value)
        {
            return value is MessageDigest md
                && (GetHashCode() == md.GetHashCode() || ToString() == md.ToString());
        }

        public static MessageDigest operator +(MessageDigest left, MessageDigest right)
        {
            return new MessageDigest
            {
                A = left.A + right.A,
                B = left.B + right.B,
                C = left.C + right.C,
                D = left.D + right.D
            };
        }

        private static string ToByteString(UInt32 x)
        {
            return string.Join(string.Empty, BitConverter.GetBytes(x).Select(y => y.ToString("x2")));
        }
    }
}
