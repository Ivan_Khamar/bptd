﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Laba4_RSA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string stringFilePath;

        private const string OpenEncipherFileTitle = "Filepath to be encipherd";
        private const string OpenDecipherFileRC5Title = "Filepath to be decipherd RC5";
        private const string OpenDecipherFileRSATitle = "Filepath to be decipherd RSA";

        private string _filepathToBeEnciphered;
        private string _filepathToBeDecipheredRC5;
        private string _filepathToBeDecipheredRSA;
        /*public enum KeyLengthInBytesEnum
        {
            Bytes_8 = 8,
            Bytes_16 = 16,
            Bytes_32 = 32
        }*/

        private const int EncipherBlockSizeRSA = 64;
        private const int DecipherBlockSizeRSA = 128;

        private readonly RC5 _rc5;
        private readonly RSACryptoServiceProvider _rsa;

        private const ByteArrayExtensions.KeyLengthInBytesEnum KeyLength = ByteArrayExtensions.KeyLengthInBytesEnum.Bytes_16;
        public MainWindow()
        {
            InitializeComponent();

            _rc5 = new RC5(new AlgorithmSettings
            {
                RoundCount = RoundCountEnum.Rounds_8,
                WordLengthInBits = WordLengthInBitsEnum.Bit32
            });
            _rsa = new RSACryptoServiceProvider();
        }

        private void fileToEncipher_Click(object sender, RoutedEventArgs e)
        {
            if (TryOpenFile(OpenEncipherFileTitle, out _filepathToBeEnciphered))
            {
                fileToEncipherPathTextBox.Text = _filepathToBeEnciphered;

                LogMessage($"{OpenEncipherFileTitle}: '{_filepathToBeEnciphered}'");
            }
        }

        private void fileToDecipherRC5_Click(object sender, RoutedEventArgs e)
        {
            if (TryOpenFile(OpenDecipherFileRC5Title, out _filepathToBeDecipheredRC5))
            {
                RC5DecipheredTextFilePathTextBox.Text = _filepathToBeDecipheredRC5;

                LogMessage($"{OpenDecipherFileRC5Title}: '{_filepathToBeDecipheredRC5}'");
            }
        }

        private void fileToDecipherRSA_Click(object sender, RoutedEventArgs e)
        {
            if (TryOpenFile(OpenDecipherFileRSATitle, out _filepathToBeDecipheredRSA))
            {
                RSADecipheredTextFilePathTextBox.Text = _filepathToBeDecipheredRSA;

                LogMessage($"{OpenDecipherFileRSATitle}: '{_filepathToBeDecipheredRSA}'");
            }
        }

        private void EncipherButton_Click(object sender, RoutedEventArgs e)
        {
            logsTextBox.Clear();
            if (!File.Exists(_filepathToBeEnciphered))
            {
                var exMsg = "Failed to encipher, file not chosen.";
                LogMessage(exMsg, "ERROR");
                MessageBox.Show(exMsg, "Laba4_RSA");

                return;
            }

            try
            {
                ProcessEncipher();
            }
            catch (Exception ex)
            {
                LogMessage(ex.Message, "ERROR");
                LogMessage("Process Encipher failed", "ERROR");
            }
        }

        private async void encipherPrivate_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(_filepathToBeEnciphered))
            {
                var exMsg = "Failed to encipher, file not chosen.";
                LogMessage(exMsg, "ERROR");
                MessageBox.Show(exMsg, "Laba4_RSA");

                return;
            }

            try
            {
                var inputBytes = File.ReadAllBytes(_filepathToBeEnciphered);
                LogMessage(
                     $"{inputBytes.Length} bytes read from file: {_filepathToBeEnciphered}.",
                     "INFO");

                var stopWatch = new Stopwatch();
                var encipheredBytes = new List<byte>
                {
                    Capacity = inputBytes.Length * 2
                };

                stopWatch.Start();

                await Task.Run(() =>
                {
                    for (int i = 0; i < inputBytes.Length; i += EncipherBlockSizeRSA)
                    {
                        var inputBlock = inputBytes
                            .Skip(i)
                            .Take(EncipherBlockSizeRSA)
                            .ToArray();

                        encipheredBytes.AddRange(_rsa.PrivateEncipher(inputBlock));
                    }
                });

                stopWatch.Stop();

                LogMessage(
                    $"RSA enciphered in {stopWatch.ElapsedMilliseconds} ms.",
                    "INFO");

                SaveBytesToFile(
                    encipheredBytes.ToArray(),
                    "Save RSA private enciphered file",
                    _filepathToBeEnciphered,
                    "-enc-pr-rsa");
            }
            catch (Exception ex)
            {
                LogMessage(ex.Message, "ERROR");
                LogMessage("Private encipher failed", "ERROR");
            }
        }

        private void Decipher_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(_filepathToBeDecipheredRC5)
               || !File.Exists(_filepathToBeDecipheredRSA))
            {
                var exMsg = "Failed to decipher, files to decipher not chosen.";
                LogMessage(exMsg, "ERROR");
                MessageBox.Show(exMsg, "Laba4_RSA");

                return;
            }

            try
            {
                ProcessDecipher();
            }
            catch (Exception ex)
            {
                LogMessage(ex.Message, "ERROR");
                LogMessage("Process Decipher failed", "ERROR");
            }
        }

        private async void ProcessDecipher()
        {
            var stopWatch = new Stopwatch();
            var hashedKey = Encoding.UTF8
                .GetBytes(RC5KeyTextBox.Text)
                .GetMD5HashedKeyForRC5(KeyLength);

            var bytesToBeDecipheredRC5 = File.ReadAllBytes(_filepathToBeDecipheredRC5);
            LogMessage(
                 $"{bytesToBeDecipheredRC5.Length} bytes read from file: {_filepathToBeDecipheredRC5}.",
                 "INFO");

            var bytesToBeDecipheredRSA = File.ReadAllBytes(_filepathToBeDecipheredRSA);
            LogMessage(
                 $"{bytesToBeDecipheredRSA.Length} bytes read from file: {_filepathToBeDecipheredRSA}.",
                 "INFO");

            stopWatch.Start();
            var decodedFileContent = _rc5.DecipherCBCPAD(
                bytesToBeDecipheredRC5,
                hashedKey);
            stopWatch.Stop();
            LogMessage(
                $"RC5 deciphered in {stopWatch.ElapsedMilliseconds} ms.",
                "INFO");

            var rsaDecipheredBytes = await DecipherRSAAsync(bytesToBeDecipheredRSA);

            SaveBytesToFile(
                decodedFileContent,
                "Save RC5 deciphered file",
                _filepathToBeDecipheredRC5,
                "-dec-rc5");

            SaveBytesToFile(
                rsaDecipheredBytes,
                "Save RSA enciphered file",
                _filepathToBeDecipheredRSA,
                "-dec-rsa");
        }

        private async Task<Byte[]> DecipherRSAAsync(Byte[] inputBytes)
        {
            var stopWatch = new Stopwatch();
            var decipheredBytes = new List<byte>
            {
                Capacity = inputBytes.Length / 2
            };

            stopWatch.Start();

            await Task.Run(() =>
            {
                for (int i = 0; i < inputBytes.Length; i += DecipherBlockSizeRSA)
                {
                    var inputBlock = inputBytes
                        .Skip(i)
                        .Take(DecipherBlockSizeRSA)
                        .ToArray();

                    decipheredBytes.AddRange(_rsa.Decrypt(
                        inputBlock,
                        fOAEP: false));
                }
            });

            stopWatch.Stop();

            LogMessage(
                $"RSA deciphered in {stopWatch.ElapsedMilliseconds} ms.",
                "INFO");

            return decipheredBytes.ToArray();
        }
        private async void decipherPrivate_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(_filepathToBeDecipheredRSA))
            {
                var exMsg = "Failed to decipher, file not chosen.";
                LogMessage(exMsg, "ERROR");
                MessageBox.Show(exMsg, "Laba4_RSA");

                return;
            }

            try
            {
                var inputBytes = File.ReadAllBytes(_filepathToBeDecipheredRSA);
                LogMessage(
                     $"{inputBytes.Length} bytes read from file: {_filepathToBeDecipheredRSA}.",
                     "INFO");

                var stopWatch = new Stopwatch();
                var decipheredBytes = new List<byte>();

                stopWatch.Start();

                await Task.Run(() =>
                {
                    for (int i = 0; i < inputBytes.Length; i += DecipherBlockSizeRSA)
                    {
                        var inputBlock = inputBytes
                            .Skip(i)
                            .Take(DecipherBlockSizeRSA)
                            .ToArray();

                        decipheredBytes.AddRange(_rsa.PublicDecipher(inputBlock));
                    }
                });

                stopWatch.Stop();

                LogMessage(
                    $"RSA deciphered in {stopWatch.ElapsedMilliseconds} ms.",
                    "INFO");

                SaveBytesToFile(
                    decipheredBytes.ToArray(),
                    "Save RSA private deciphered file",
                    _filepathToBeEnciphered,
                    "-dec-pr-rsa");
            }
            catch (Exception ex)
            {
                LogMessage(ex.Message, "ERROR");
                LogMessage("Private encipher failed", "ERROR");
            }
        }
        private void exportRSAKey_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //using (var saveFileDlg = new SaveFileDialog())
                //{
                    SaveFileDialog saveFileDlg = new SaveFileDialog();
                    saveFileDlg.Title = "Export RSA keys";
                    saveFileDlg.FileName = "RSA_Keys.xml";

                    var dlgRes = saveFileDlg.ShowDialog();

                    if (dlgRes == true)
                    {
                        File.WriteAllText(
                            saveFileDlg.FileName,
                            _rsa.ToXmlString(includePrivateParameters: true));

                        LogMessage($"File '{saveFileDlg.FileName}' saved.", "INFO");
                    }
                    else
                    {
                        LogMessage($"File save cancelled.", "WARNING");
                    }
                //}
            }
            catch (Exception ex)
            {
                LogMessage(ex.Message, "ERROR");
                LogMessage("Export RSA keys failed", "ERROR");
            }
        }

        private void importRSAKey_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!TryOpenFile("Import RSA keys", out var rsaKeysPath))
                    return;

                _rsa.FromXmlString(File.ReadAllText(rsaKeysPath));

                LogMessage($"Successfully imported RSA keys from '{rsaKeysPath}'");
            }
            catch (Exception ex)
            {
                LogMessage(ex.Message, "ERROR");
                LogMessage("Import RSA keys failed", "ERROR");
            }
        }

        private void clearLogs_Click(object sender, RoutedEventArgs e)
        {
            logsTextBox.Clear();
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////

        private void LogMessage(string message, string logLevel = "INFO")
        {
            logsTextBox.AppendText(
                $"{/*DateTime.Now.ToLongTimeString()*/""} {/*logLevel*/""}> {message}\n");
                //$"{/*DateTime.Now.ToLongTimeString()*/""} [{/*logLevel*/""}]: {message}\n");
        }

        private bool TryOpenFile(string dlgTitle, out string filepath)
        {
            /*bool isFilenameRetrieved = false;
            filepath = null;
            
            OpenFileDialog openFileDialogInstance = new OpenFileDialog();
            //openFileDialogInstance.Filter = "Text|*.txt|All|*.*";
            //openFileDialogInstance.FilterIndex = 1;
            //openFileDialogInstance.Multiselect = false;
            openFileDialogInstance.Title = dlgTitle;
            openFileDialogInstance.RestoreDirectory = true;
            //string filePath = null;
            //string[] filesPathsArray = null;

            if (openFileDialogInstance.ShowDialog() == true)
            {
                //filePath = openFileDialogInstance.FileName;
                //filesPathsArray = openFileDialogInstance.FileNames;
                filepath = openFileDialogInstance.FileName;
                isFilenameRetrieved = true;
            }

            //string textInFile = File.ReadAllText(filePath);
            //fileTextBox.Text = filePath;
            //stringFilePath = filePath;
             

            /*using (var openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Title = dlgTitle;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filepath = openFileDialog.FileName;
                    isFilenameRetrieved = true;
                }
            }*/

            //return isFilenameRetrieved;*/

            bool isFilenameRetrieved = false;
            filepath = null;

            //using (var openFileDialog = new OpenFileDialog())
            //{
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Title = dlgTitle;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == true)
                {
                    filepath = openFileDialog.FileName;
                    isFilenameRetrieved = true;
                }
            //}

            return isFilenameRetrieved;
        }


        private async void ProcessEncipher()
        {
            var stopWatch = new Stopwatch();
            var rc5HashedKey = Encoding.UTF8
                .GetBytes(RC5KeyTextBox.Text)
                .GetMD5HashedKeyForRC5(KeyLength);

            var inputBytes = File.ReadAllBytes(_filepathToBeEnciphered);
            LogMessage(
                 $"{inputBytes.Length} bytes read from file: {_filepathToBeEnciphered}.",
                 "INFO");

            stopWatch.Start();
            var rc5EncipheredBytes = _rc5.EncipherCBCPAD(
                inputBytes,
                rc5HashedKey);
            stopWatch.Stop();
            LogMessage(
                $"RC5 enciphered in {stopWatch.ElapsedMilliseconds} ms.",
                "INFO");

            var rsaEncipheredBytes = await EncipherRSAAsync(inputBytes);

            SaveBytesToFile(
                rc5EncipheredBytes,
                "Save RC5 enciphered file",
                _filepathToBeEnciphered,
                "-enc-rc5");

            SaveBytesToFile(
                rsaEncipheredBytes,
                "Save RSA enciphered file",
                _filepathToBeEnciphered,
                "-enc-rsa");
        }

        private async Task<Byte[]> EncipherRSAAsync(Byte[] inputBytes)
        {
            var stopWatch = new Stopwatch();
            var encipheredBytes = new List<byte>
            {
                Capacity = inputBytes.Length * 2
            };

            stopWatch.Start();

            await Task.Run(() =>
            {
                for (int i = 0; i < inputBytes.Length; i += EncipherBlockSizeRSA)
                {
                    var inputBlock = inputBytes
                        .Skip(i)
                        .Take(EncipherBlockSizeRSA)
                        .ToArray();

                    encipheredBytes.AddRange(_rsa.Encrypt(
                        inputBlock,
                        fOAEP: false));
                }
            });

            stopWatch.Stop();

            LogMessage(
                $"RSA enciphered in {stopWatch.ElapsedMilliseconds} ms.",
                "INFO");

            return encipheredBytes.ToArray();
        }

        private void SaveBytesToFile(
            Byte[] rc5EncipheredBytes,
            string dlgTitle,
            string oldFileName,
            string filenamePadding)
        {
            /*
              string filePath = null;

                    SaveFileDialog saveHashInFileDialog = new SaveFileDialog();
                    saveHashInFileDialog.Filter = "Text|*.txt|All|*.*";
                    saveHashInFileDialog.FilterIndex = 1;

                    if (saveHashInFileDialog.ShowDialog() == true)
                    {
                        filePath = saveHashInFileDialog.FileName;
                    }

                    File.WriteAllText(filePath, (string)hashLabel.Content);
                    saveHashFileLabel.Content = filePath;
             */

            //using (var saveFileDlg = new SaveFileDialog())
            //{
                SaveFileDialog saveFileDlg = new SaveFileDialog();
                saveFileDlg.Title = dlgTitle;
                saveFileDlg.FileName = PaddFilename(
                    oldFileName,
                    filenamePadding);

                var dlgRes = saveFileDlg.ShowDialog();

                if (dlgRes == true)
                {
                    File.WriteAllBytes(
                        saveFileDlg.FileName,
                        rc5EncipheredBytes);

                    LogMessage($"File '{saveFileDlg.FileName}' saved.", "INFO");
                }
                else
                {
                    LogMessage($"File save cancelled.", "WARNING");
                }
            //}
        }

        private static string PaddFilename(string filePath, string padding)
        {
            var fi = new FileInfo(filePath);
            var fn = System.IO.Path.GetFileNameWithoutExtension(filePath);

            return System.IO.Path.Combine(
                fi.DirectoryName,
                fn + padding + fi.Extension);
        }
    }

    /// <summary>
    /// ////////////////////////////////////////////////////////////////////////
    /// </summary>
    public static class ByteArrayExtensions
    {
        public enum KeyLengthInBytesEnum
        {
            Bytes_8 = 8,
            Bytes_16 = 16,
            Bytes_32 = 32
        }
        public static Byte[] GetMD5HashedKeyForRC5(
            this Byte[] key,
            KeyLengthInBytesEnum keyLengthInBytes)
        {
            if (key is null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            var hasher = new MD5();
            var bytesHash = hasher.ComputeHash(key).ToByteArray();

            if (keyLengthInBytes == KeyLengthInBytesEnum.Bytes_8)
            {
                bytesHash = bytesHash.Take(bytesHash.Length / 2).ToArray();
            }
            else if (keyLengthInBytes == KeyLengthInBytesEnum.Bytes_32)
            {
                bytesHash = bytesHash
                    .Concat(hasher.ComputeHash(bytesHash).ToByteArray())
                    .ToArray();
            }

            if (bytesHash.Length != (int)keyLengthInBytes)
            {
                throw new InvalidOperationException(
                    $"Internal error at {nameof(ByteArrayExtensions.GetMD5HashedKeyForRC5)} method, " +
                    $"hash result is not equal to {(int)keyLengthInBytes}.");
            }

            return bytesHash;
        }

        internal static void XorWith(
            this Byte[] array,
            Byte[] xorArray,
            int inStartIndex,
            int xorStartIndex,
            int length)
        {
            for (int i = 0; i < length; ++i)
            {
                array[i + inStartIndex] ^= xorArray[i + xorStartIndex];
            }
        }
    }

    public sealed class MD5
    {
        private const int OptimalChunkSizeMultiplier = 100_000;
        private const UInt32 OptimalChunkSize = MD5C.BytesCountPerBits512Block * OptimalChunkSizeMultiplier;

        /// <summary>
        /// Computed hash as <see cref="string"/>
        /// </summary>
        public string HashAsString => Hash.ToString();

        /// <summary>
        /// Computed hash as <see cref="MessageDigest"/> model
        /// </summary>
        public MessageDigest Hash { get; private set; }

        /// <summary>
        /// Computes MD5 hash of input message
        /// </summary>
        /// <param name="message">Input message</param>
        /// <returns>Returns hash as <see cref="MessageDigest"/> model</returns>
        public void ComputeHash(string message)
        {
            ComputeHash(Encoding.ASCII.GetBytes(message));
        }

        /// <summary>
        /// Computes MD5 hash of input message
        /// </summary>
        /// <param name="message">Input message, byte array</param>
        /// <returns>Returns hash as <see cref="MessageDigest"/> model</returns>
        public MessageDigest ComputeHash(Byte[] message)
        {
            Hash = MessageDigest.InitialValue;

            var paddedMessage = JoinArrays(message, GetMessagePadding((UInt32)message.Length));

            for (UInt32 bNo = 0; bNo < paddedMessage.Length / MD5C.BytesCountPerBits512Block; ++bNo)
            {
                UInt32[] X = BitsHelper.Extract32BitWords(
                    paddedMessage,
                    bNo,
                    MD5C.Words32BitArraySize * MD5C.BytesPer32BitWord);

                FeedMessageBlockToBeHashed(X);
            }

            return Hash;
        }

        /// <summary>
        /// Computes MD5 hash of input file
        /// </summary>
        /// <param name="filePath">Path to file to be hashed</param>
        /// <param name="chunkSizeMultiplier"></param>
        /// <returns></returns>
        public async Task<MessageDigest> ComputeFileHashAsync(String filePath)
        {
            Hash = MessageDigest.InitialValue;

            using (var fs = File.OpenRead(filePath))
            {
                UInt64 totalBytesRead = 0;
                Int32 currentBytesRead = 0;
                bool isFileEnd = false;

                do
                {
                    var chunk = new Byte[OptimalChunkSize];

                    currentBytesRead = await fs.ReadAsync(chunk, 0, chunk.Length);
                    totalBytesRead += (UInt64)currentBytesRead;


                    if (currentBytesRead < chunk.Length)
                    {
                        Byte[] lastChunk;

                        if (currentBytesRead == 0)
                        {
                            lastChunk = GetMessagePadding(totalBytesRead);
                        }
                        else
                        {
                            lastChunk = new Byte[currentBytesRead];
                            Array.Copy(chunk, lastChunk, currentBytesRead);

                            lastChunk = JoinArrays(lastChunk, GetMessagePadding(totalBytesRead));
                        }

                        chunk = lastChunk;
                        isFileEnd = true;
                    }

                    for (UInt32 bNo = 0; bNo < chunk.Length / MD5C.BytesCountPerBits512Block; ++bNo)
                    {
                        UInt32[] X = BitsHelper.Extract32BitWords(
                            chunk,
                            bNo,
                            MD5C.Words32BitArraySize * MD5C.BytesPer32BitWord);

                        FeedMessageBlockToBeHashed(X);
                    }
                }
                while (isFileEnd == false);
            }

            return Hash;
        }

        private void FeedMessageBlockToBeHashed(UInt32[] X)
        {
            UInt32 F, i, k;
            var blockSize = MD5C.BytesCountPerBits512Block;
            var MDq = Hash.Clone();

            // first round
            for (i = 0; i < blockSize / 4; ++i)
            {
                k = i;
                F = BitsHelper.FuncF(MDq.B, MDq.C, MDq.D);

                MDq.MD5IterationSwap(F, X, i, k);
            }
            // second round
            for (; i < blockSize / 2; ++i)
            {
                k = (1 + (5 * i)) % (blockSize / 4);
                F = BitsHelper.FuncG(MDq.B, MDq.C, MDq.D);

                MDq.MD5IterationSwap(F, X, i, k);
            }
            // third round
            for (; i < blockSize / 4 * 3; ++i)
            {
                k = (5 + (3 * i)) % (blockSize / 4);
                F = BitsHelper.FuncH(MDq.B, MDq.C, MDq.D);

                MDq.MD5IterationSwap(F, X, i, k);
            }
            // fourth round
            for (; i < blockSize; ++i)
            {
                k = 7 * i % (blockSize / 4);
                F = BitsHelper.FuncI(MDq.B, MDq.C, MDq.D);

                MDq.MD5IterationSwap(F, X, i, k);
            }

            Hash += MDq;
        }

        private static Byte[] GetMessagePadding(UInt64 messageLength)
        {
            UInt32 paddingLengthInBytes = default;
            var mod = (UInt32)(messageLength * MD5C.BitsPerByte % MD5C.Bits512BlockSize);

            // Append Padding Bits
            if (mod == MD5C.BITS_448)
            {
                paddingLengthInBytes = MD5C.Bits512BlockSize / MD5C.BitsPerByte;
            }
            else if (mod > MD5C.BITS_448)
            {
                paddingLengthInBytes = (MD5C.Bits512BlockSize - mod + MD5C.BITS_448) / MD5C.BitsPerByte;
            }
            else if (mod < MD5C.BITS_448)
            {
                paddingLengthInBytes = (MD5C.BITS_448 - mod) / MD5C.BitsPerByte;
            }

            var padding = new Byte[paddingLengthInBytes + MD5C.BitsPerByte];
            padding[0] = MD5C.BITS_128;

            // Append Length
            var messageLength64bit = messageLength * MD5C.BitsPerByte;

            for (var i = 0; i < MD5C.BitsPerByte; ++i)
            {
                padding[paddingLengthInBytes + i] = (Byte)(messageLength64bit
                    >> (Int32)(i * MD5C.BitsPerByte)
                    & MD5C.BITS_255);
            }

            return padding;
        }

        private static T[] JoinArrays<T>(T[] first, T[] second)
        {
            T[] joinedArray = new T[first.Length + second.Length];
            Array.Copy(first, joinedArray, first.Length);
            Array.Copy(second, 0, joinedArray, first.Length, second.Length);

            return joinedArray;
        }
    }

    internal static class MD5C
    {
        public const UInt32 BitsPerByte = 8;
        public const UInt32 BytesPer32BitWord = 4;
        public const UInt32 Words32BitArraySize = 16;
        public const UInt32 Bits512BlockSize = 512u;
        public const UInt32 BytesCountPerBits512Block = Bits512BlockSize / BitsPerByte;
        public const UInt32 BITS_448 = 448u;
        public const Byte BITS_255 = 0b11111111;
        public const Byte BITS_128 = 0b10000000;

        /// <summary>
        /// Initial value for A word of MD buffer
        /// </summary>
        public const UInt32 A_MD_BUFFER_INITIAL = 0x67452301;

        /// <summary>
        /// Initial value for B word of MD buffer
        /// </summary>
        public const UInt32 B_MD_BUFFER_INITIAL = 0xefcdab89;

        /// <summary>
        /// Initial value for C word of MD buffer
        /// </summary>
        public const UInt32 C_MD_BUFFER_INITIAL = 0x98badcfe;

        /// <summary>
        /// Initial value for D word of MD buffer
        /// </summary>
        public const UInt32 D_MD_BUFFER_INITIAL = 0x10325476;

        /// <summary>
        /// Lookup table with values based on sin() values
        /// </summary>
        public readonly static UInt32[] T = new UInt32[64]
        {
            0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
            0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
            0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
            0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
            0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
            0xd62f105d, 0x2441453,  0xd8a1e681, 0xe7d3fbc8,
            0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
            0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
            0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
            0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
            0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x4881d05,
            0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
            0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
            0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
            0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
            0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
        };

        /// <summary>
        /// Lookup table with round shift values
        /// </summary>
        public readonly static Int32[] S = new Int32[] {
            7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
            5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
            4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
            6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21
        };
    }
    internal static class BitsHelper
    {
        /// <summary>
        /// Extracts <see langword="UInt32"/> words array
        /// </summary>
        public static UInt32[] Extract32BitWords(Byte[] message, UInt32 blockNo, UInt32 blockSizeInBytes)
        {
            var messageStartIndex = blockNo * blockSizeInBytes;
            var extractedArray = new UInt32[blockSizeInBytes / MD5C.BytesPer32BitWord];

            for (UInt32 i = 0; i < blockSizeInBytes; i += MD5C.BytesPer32BitWord)
            {
                var j = messageStartIndex + i;

                extractedArray[i / MD5C.BytesPer32BitWord] = // form 32-bit word from four bytes
                      message[j]                                                   // first byte
                    | (((UInt32)message[j + 1]) << ((Int32)MD5C.BitsPerByte * 1))  // second byte
                    | (((UInt32)message[j + 2]) << ((Int32)MD5C.BitsPerByte * 2))  // third byte
                    | (((UInt32)message[j + 3]) << ((Int32)MD5C.BitsPerByte * 3)); // fourth byte
            }

            return extractedArray;
        }

        /// <summary>
        /// Shifts value bits for <paramref name="shiftValue"/>
        /// </summary>
        public static UInt32 LeftRotate(UInt32 value, Int32 shiftValue)
        {
            return (value << shiftValue)
                 | (value >> (Int32)(MD5C.BitsPerByte * MD5C.BytesPer32BitWord - shiftValue));
        }

        /// <summary>
        /// Elementary function F(B, C, D)
        /// </summary>
        public static UInt32 FuncF(UInt32 B, UInt32 C, UInt32 D) => (B & C) | (~B & D);

        /// <summary>
        /// Elementary function G(B, C, D)
        /// </summary>
        public static UInt32 FuncG(UInt32 B, UInt32 C, UInt32 D) => (D & B) | (C & ~D);

        /// <summary>
        /// Elementary function H(B, C, D)
        /// </summary>
        public static UInt32 FuncH(UInt32 B, UInt32 C, UInt32 D) => B ^ C ^ D;

        /// <summary>
        /// Elementary function I(B, C, D)
        /// </summary>
        public static UInt32 FuncI(UInt32 B, UInt32 C, UInt32 D) => C ^ (B | ~D);
    }

    public class MessageDigest
    {
        internal static MessageDigest InitialValue { get; }

        static MessageDigest()
        {
            InitialValue = new MessageDigest
            {
                A = MD5C.A_MD_BUFFER_INITIAL,
                B = MD5C.B_MD_BUFFER_INITIAL,
                C = MD5C.C_MD_BUFFER_INITIAL,
                D = MD5C.D_MD_BUFFER_INITIAL
            };
        }

        public UInt32 A { get; set; }

        public UInt32 B { get; set; }

        public UInt32 C { get; set; }

        public UInt32 D { get; set; }

        public Byte[] ToByteArray()
        {
            return ArraysHelper.ConcatArrays(
                BitConverter.GetBytes(A),
                BitConverter.GetBytes(B),
                BitConverter.GetBytes(C),
                BitConverter.GetBytes(D));
        }

        public MessageDigest Clone()
        {
            return MemberwiseClone() as MessageDigest;
        }

        internal void MD5IterationSwap(UInt32 F, UInt32[] X, UInt32 i, UInt32 k)
        {
            var tempD = D;
            D = C;
            C = B;
            B += BitsHelper.LeftRotate(A + F + X[k] + MD5C.T[i], MD5C.S[i]);
            A = tempD;
        }

        public override String ToString()
        {
            return $"{ToByteString(A)}{ToByteString(B)}{ToByteString(C)}{ToByteString(D)}";
        }

        public override Int32 GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override Boolean Equals(Object value)
        {
            return value is MessageDigest md
                && (GetHashCode() == md.GetHashCode() || ToString() == md.ToString());
        }

        public static MessageDigest operator +(MessageDigest left, MessageDigest right)
        {
            return new MessageDigest
            {
                A = left.A + right.A,
                B = left.B + right.B,
                C = left.C + right.C,
                D = left.D + right.D
            };
        }

        private static string ToByteString(UInt32 x)
        {
            return string.Join(string.Empty, BitConverter.GetBytes(x).Select(y => y.ToString("x2")));
        }
    }

    public static class ArraysHelper
    {
        public static T[] ConcatArrays<T>(params T[][] arrays)
        {
            var position = 0;
            var outputArray = new T[arrays.Sum(a => a.Length)];

            foreach (var a in arrays)
            {
                Array.Copy(a, 0, outputArray, position, a.Length);
                position += a.Length;
            }

            return outputArray;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////

    public class RC5
    {
        private readonly IPseudoRandomNumberGenerator _numberGenerator;
        private readonly IWordFactory _wordsFactory;
        private readonly Int32 _roundsCount;

        public RC5(AlgorithmSettings algorithmSettings)
        {
            _numberGenerator = new PseudoRandomNumberGenerator(
                PseudoRandomNumberGeneratorOptions.Optimal);

            _wordsFactory = algorithmSettings.WordLengthInBits.GetWordFactory();
            _roundsCount = (int)algorithmSettings.RoundCount;
        }

        public Byte[] EncipherCBCPAD(Byte[] input, Byte[] key)
        {
            var paddedBytes = ArraysHelper.ConcatArrays(input, GetPadding(input));
            var bytesPerBlock = _wordsFactory.BytesPerBlock;
            var s = BuildExpandedKeyTable(key);
            var cnPrev = GetRandomBytesForInitVector().Take(bytesPerBlock).ToArray();
            var encodedFileContent = new Byte[cnPrev.Length + paddedBytes.Length];

            EncipherECB(cnPrev, encodedFileContent, inStart: 0, outStart: 0, s);

            for (int i = 0; i < paddedBytes.Length; i += bytesPerBlock)
            {
                var cn = new Byte[bytesPerBlock];
                Array.Copy(paddedBytes, i, cn, 0, cn.Length);

                cn.XorWith(
                    xorArray: cnPrev,
                    inStartIndex: 0,
                    xorStartIndex: 0,
                    length: cn.Length);

                EncipherECB(
                    inBytes: cn,
                    outBytes: encodedFileContent,
                    inStart: 0,
                    outStart: i + bytesPerBlock,
                    s: s);

                Array.Copy(encodedFileContent, i + bytesPerBlock, cnPrev, 0, cn.Length);
            }

            return encodedFileContent;
        }

        public Byte[] DecipherCBCPAD(Byte[] input, Byte[] key)
        {
            var bytesPerBlock = _wordsFactory.BytesPerBlock;
            var s = BuildExpandedKeyTable(key);
            var cnPrev = new Byte[bytesPerBlock];
            var decodedFileContent = new Byte[input.Length - cnPrev.Length];

            DecipherECB(
                inBuf: input,
                outBuf: cnPrev,
                inStart: 0,
                outStart: 0,
                s: s);

            for (int i = bytesPerBlock; i < input.Length; i += bytesPerBlock)
            {
                var cn = new Byte[bytesPerBlock];
                Array.Copy(input, i, cn, 0, cn.Length);

                DecipherECB(
                    inBuf: cn,
                    outBuf: decodedFileContent,
                    inStart: 0,
                    outStart: i - bytesPerBlock,
                    s: s);

                decodedFileContent.XorWith(
                    xorArray: cnPrev,
                    inStartIndex: i - bytesPerBlock,
                    xorStartIndex: 0,
                    length: cn.Length);

                Array.Copy(input, i, cnPrev, 0, cnPrev.Length);
            }

            var decodedWithoutPadding = new Byte[decodedFileContent.Length - decodedFileContent.Last()];
            Array.Copy(decodedFileContent, decodedWithoutPadding, decodedWithoutPadding.Length);

            return decodedWithoutPadding;
        }

        private void EncipherECB(Byte[] inBytes, Byte[] outBytes, Int32 inStart, Int32 outStart, IWord[] s)
        {
            var a = _wordsFactory.CreateFromBytes(inBytes, inStart);
            var b = _wordsFactory.CreateFromBytes(inBytes, inStart + _wordsFactory.BytesPerWord);

            a.Add(s[0]);
            b.Add(s[1]);

            for (var i = 1; i < _roundsCount + 1; ++i)
            {
                a.XorWith(b).ROL(b.ToInt32()).Add(s[2 * i]);
                b.XorWith(a).ROL(a.ToInt32()).Add(s[2 * i + 1]);
            }

            a.FillBytesArray(outBytes, outStart);
            b.FillBytesArray(outBytes, outStart + _wordsFactory.BytesPerWord);
        }

        private void DecipherECB(Byte[] inBuf, Byte[] outBuf, Int32 inStart, Int32 outStart, IWord[] s)
        {
            var a = _wordsFactory.CreateFromBytes(inBuf, inStart);
            var b = _wordsFactory.CreateFromBytes(inBuf, inStart + _wordsFactory.BytesPerWord);

            for (var i = _roundsCount; i > 0; --i)
            {
                b = b.Sub(s[2 * i + 1]).ROR(a.ToInt32()).XorWith(a);
                a = a.Sub(s[2 * i]).ROR(b.ToInt32()).XorWith(b);
            }

            a.Sub(s[0]);
            b.Sub(s[1]);

            a.FillBytesArray(outBuf, outStart);
            b.FillBytesArray(outBuf, outStart + _wordsFactory.BytesPerWord);
        }

        private Byte[] GetPadding(Byte[] inBytes)
        {
            var paddingLength = _wordsFactory.BytesPerBlock - inBytes.Length % (_wordsFactory.BytesPerBlock);

            var padding = new Byte[paddingLength];

            for (int i = 0; i < padding.Length; ++i)
            {
                padding[i] = (Byte)paddingLength;
            }

            return padding;
        }

        private Byte[] GetRandomBytesForInitVector()
        {
            var ivParts = new List<Byte[]>();

            while (ivParts.Sum(ivp => ivp.Length) < _wordsFactory.BytesPerBlock)
            {
                ivParts.Add(BitConverter.GetBytes(_numberGenerator.NextNumber()));
            }

            return ArraysHelper.ConcatArrays(ivParts.ToArray());
        }

        private IWord[] BuildExpandedKeyTable(Byte[] key)
        {
            var keysWordArrLength = key.Length % _wordsFactory.BytesPerWord > 0
                ? key.Length / _wordsFactory.BytesPerWord + 1
                : key.Length / _wordsFactory.BytesPerWord;

            var lArr = new IWord[keysWordArrLength];

            for (int i = 0; i < lArr.Length; i++)
            {
                lArr[i] = _wordsFactory.Create();
            }

            for (var i = key.Length - 1; i >= 0; i--)
            {
                lArr[i / _wordsFactory.BytesPerWord].ROL(RC5Constants.BitsPerByte).Add(key[i]);
            }

            var sArray = new IWord[2 * (_roundsCount + 1)];
            sArray[0] = _wordsFactory.CreateP();
            var q = _wordsFactory.CreateQ();

            for (var i = 1; i < sArray.Length; i++)
            {
                sArray[i] = sArray[i - 1].Clone();
                sArray[i].Add(q);
            }

            var x = _wordsFactory.Create();
            var y = _wordsFactory.Create();

            var n = 3 * Math.Max(sArray.Length, lArr.Length);

            for (Int32 k = 0, i = 0, j = 0; k < n; ++k)
            {
                sArray[i].Add(x).Add(y).ROL(3);
                x = sArray[i].Clone();

                lArr[j].Add(x).Add(y).ROL(x.ToInt32() + y.ToInt32());
                y = lArr[j].Clone();

                i = (i + 1) % sArray.Length;
                j = (j + 1) % lArr.Length;
            }

            return sArray;
        }
    }

    internal interface IWord
    {
        void CreateFromBytes(byte[] bytes, Int32 startFromIndex);
        Byte[] FillBytesArray(Byte[] bytesToFill, Int32 startFromIndex);
        IWord ROL(Int32 offset);
        IWord ROR(Int32 offset);
        IWord XorWith(IWord word);
        IWord Add(IWord word);
        IWord Add(byte value);
        IWord Sub(IWord word);
        IWord Clone();
        Int32 ToInt32();
    }

    public static class RC5Constants
    {
        public const UInt16 P16 = 0xB7E1;
        public const UInt16 Q16 = 0x9E37;
        public const UInt32 P32 = 0xB7E15162;
        public const UInt32 Q32 = 0x9E3779B9;
        public const UInt64 P64 = 0xB7E151628AED2A6B;
        public const UInt64 Q64 = 0x9E3779B97F4A7C15;

        public const Int32 BitsPerByte = 8;
        public const Int32 ByteMask = 0b11111111;

        public const Int32 MinRoundCount = 0;
        public const Int32 MaxRoundCount = 255;
        public const Int32 MinSecretKeyOctetsCount = 0;
        public const Int32 MaxSecretKeyOctetsCount = 255;
        public const Int32 MinKeySizeInBytes = 0;
        public const Int32 MaxKeySizeInBytes = 2040;
    }

    public class AlgorithmSettings
    {
        public RoundCountEnum RoundCount { get; set; }
        public WordLengthInBitsEnum WordLengthInBits { get; set; }
    }
    public enum RoundCountEnum
    {
        Rounds_8 = 8,
        Rounds_12 = 12,
        Rounds_16 = 16,
        Rounds_20 = 20
    }
    public enum WordLengthInBitsEnum
    {
        Bit16,
        Bit32,
        Bit64
    }

    public interface IPseudoRandomNumberGenerator
    {
        ulong NextNumber();
        ulong NextNumber(ulong currentNumber);
        void Reset();
    }

    internal interface IWordFactory
    {
        Int32 BytesPerWord { get; }
        Int32 BytesPerBlock { get; }
        IWord CreateQ();
        IWord CreateP();
        IWord Create();
        IWord CreateFromBytes(byte[] bytes, Int32 startFrom);
    }

    public class PseudoRandomNumberGenerator : IPseudoRandomNumberGenerator
    {
        private ulong _currentNumber;
        private readonly ulong _mod;
        private readonly ulong _multiplier;
        private readonly ulong _cummulative;
        private readonly ulong _startValue;

        public PseudoRandomNumberGenerator(PseudoRandomNumberGeneratorOptions options)
        {
            _mod = options.Mod;
            _multiplier = options.Multiplier;
            _cummulative = options.Cummulative;
            _startValue = options.StartValue;
            _currentNumber = options.StartValue;
        }

        public ulong NextNumber()
        {
            var nextNumber = _currentNumber;

            _currentNumber = SequenceFormula(
                _multiplier,
                _cummulative,
                _mod,
                nextNumber);

            return nextNumber;
        }

        public ulong NextNumber(ulong currentNumber)
        {
            return SequenceFormula(
                _multiplier,
                _cummulative,
                _mod,
                currentNumber);
        }

        public void Reset()
        {
            _currentNumber = _startValue;
        }

        private static ulong SequenceFormula(
            ulong mult,
            ulong c,
            ulong mod,
            ulong x)
        {
            return (mult * x + c) % mod;
        }
    }

    public class PseudoRandomNumberGeneratorOptions
    {
        static PseudoRandomNumberGeneratorOptions()
        {
            Default = new PseudoRandomNumberGeneratorOptions
            {
                Mod = (ulong)Math.Pow(2, 26) - 1,
                Cummulative = 1597,
                Multiplier = (ulong)Math.Pow(13, 3),
                StartValue = 13
            };

            Optimal = new PseudoRandomNumberGeneratorOptions
            {
                Mod = (ulong)Math.Pow(2, 31) - 1,
                Cummulative = 17711,
                Multiplier = (ulong)Math.Pow(7, 5),
                StartValue = 31
            };
        }

        public static PseudoRandomNumberGeneratorOptions Default { get; }
        public static PseudoRandomNumberGeneratorOptions Optimal { get; }

        public ulong Mod { get; set; }

        public ulong Multiplier { get; set; }

        public ulong Cummulative { get; set; }

        public ulong StartValue { get; set; }
    }

    internal static class WordLengthInBitsEnumExtensions
    {
        internal static IWordFactory GetWordFactory(this WordLengthInBitsEnum wordLengthInBits)
        {
            IWordFactory wordFactory = null;

            switch (wordLengthInBits)
            {
                case WordLengthInBitsEnum.Bit16:
                    wordFactory = new Word16BitFactory();
                    break;
                case WordLengthInBitsEnum.Bit32:
                    wordFactory = new Word32BitFactory();
                    break;
                case WordLengthInBitsEnum.Bit64:
                    wordFactory = new Word64BitFactory();
                    break;
                default:
                    throw new ArgumentException($"Invalid {nameof(WordLengthInBitsEnum)} value.");
            }

            return wordFactory;
        }
    }

    internal class Word16BitFactory : IWordFactory
    {
        public Int32 BytesPerWord => Word16Bit.BytesPerWord;

        public Int32 BytesPerBlock => BytesPerWord * 2;

        public IWord Create()
        {
            return CreateConcrete();
        }

        public IWord CreateP()
        {
            return CreateConcrete(RC5Constants.P16);
        }

        public IWord CreateQ()
        {
            return CreateConcrete(RC5Constants.Q16);
        }

        public IWord CreateFromBytes(Byte[] bytes, Int32 startFromIndex)
        {
            var word = Create();
            word.CreateFromBytes(bytes, startFromIndex);

            return word;
        }

        private Word16Bit CreateConcrete(UInt16 value = 0)
        {
            return new Word16Bit
            {
                WordValue = value
            };
        }
    }

    internal class Word16Bit : IWord
    {
        public const Int32 WordSizeInBits = BytesPerWord * RC5Constants.BitsPerByte;
        public const Int32 BytesPerWord = sizeof(UInt16);

        public UInt16 WordValue { get; set; }

        public void CreateFromBytes(Byte[] bytes, Int32 startFrom)
        {
            WordValue = 0;

            for (var i = startFrom + BytesPerWord - 1; i > startFrom; --i)
            {
                WordValue = (UInt16)(WordValue | bytes[i]);
                WordValue = (UInt16)(WordValue << RC5Constants.BitsPerByte);
            }

            WordValue = (UInt16)(WordValue | bytes[startFrom]);
        }

        public Byte[] FillBytesArray(Byte[] bytesToFill, Int32 startFrom)
        {
            var i = 0;
            for (; i < BytesPerWord - 1; ++i)
            {
                bytesToFill[startFrom + i] = (Byte)(WordValue & RC5Constants.ByteMask);
                WordValue = (UInt16)(WordValue >> RC5Constants.BitsPerByte);
            }

            bytesToFill[startFrom + i] = (Byte)(WordValue & RC5Constants.ByteMask);

            return bytesToFill;
        }

        public IWord ROL(Int32 offset)
        {
            offset %= BytesPerWord;
            WordValue = (UInt16)((WordValue << offset) | (WordValue >> (WordSizeInBits - offset)));

            return this;
        }

        public IWord ROR(Int32 offset)
        {
            offset %= BytesPerWord;
            WordValue = (UInt16)((WordValue >> offset) | (WordValue << (WordSizeInBits - offset)));

            return this;
        }

        public IWord Add(IWord word)
        {
            WordValue = (UInt16)(WordValue + (word as Word16Bit).WordValue);

            return this;
        }

        public IWord Add(Byte value)
        {
            WordValue = (UInt16)(WordValue + value);

            return this;
        }

        public IWord Sub(IWord word)
        {
            WordValue = (UInt16)(WordValue - (word as Word16Bit).WordValue);

            return this;
        }

        public IWord XorWith(IWord word)
        {
            WordValue = (UInt16)(WordValue ^ (word as Word16Bit).WordValue);

            return this;
        }

        public IWord Clone()
        {
            return (IWord)MemberwiseClone();
        }

        public Int32 ToInt32()
        {
            return WordValue;
        }
    }

    internal class Word32BitFactory : IWordFactory
    {
        public Int32 BytesPerWord => Word32Bit.BytesPerWord;

        public Int32 BytesPerBlock => BytesPerWord * 2;

        public IWord Create()
        {
            return CreateConcrete();
        }

        public IWord CreateP()
        {
            return CreateConcrete(RC5Constants.P32);
        }

        public IWord CreateQ()
        {
            return CreateConcrete(RC5Constants.Q32);
        }

        public IWord CreateFromBytes(Byte[] bytes, Int32 startFromIndex)
        {
            var word = Create();
            word.CreateFromBytes(bytes, startFromIndex);

            return word;
        }

        public Word32Bit CreateConcrete(UInt32 value = 0)
        {
            return new Word32Bit
            {
                WordValue = value
            };
        }
    }

    internal class Word32Bit : IWord
    {
        public const Int32 WordSizeInBits = BytesPerWord * RC5Constants.BitsPerByte;
        public const Int32 BytesPerWord = sizeof(UInt32);

        public UInt32 WordValue { get; set; }

        public void CreateFromBytes(Byte[] bytes, Int32 startFrom)
        {
            WordValue = 0;

            for (var i = startFrom + BytesPerWord - 1; i > startFrom; --i)
            {
                WordValue |= bytes[i];
                WordValue <<= RC5Constants.BitsPerByte;
            }

            WordValue |= bytes[startFrom];
        }

        public Byte[] FillBytesArray(Byte[] bytesToFill, Int32 startFrom)
        {
            var i = 0;
            for (; i < BytesPerWord - 1; ++i)
            {
                bytesToFill[startFrom + i] = (Byte)(WordValue & RC5Constants.ByteMask);
                WordValue >>= RC5Constants.BitsPerByte;
            }

            bytesToFill[startFrom + i] = (Byte)(WordValue & RC5Constants.ByteMask);

            return bytesToFill;
        }

        public IWord ROL(Int32 offset)
        {
            WordValue = (WordValue << offset) | (WordValue >> (WordSizeInBits - offset));

            return this;
        }

        public IWord ROR(Int32 offset)
        {
            WordValue = (WordValue >> offset) | (WordValue << (WordSizeInBits - offset));

            return this;
        }

        public IWord Add(IWord word)
        {
            WordValue += (word as Word32Bit).WordValue;

            return this;
        }

        public IWord Add(Byte value)
        {
            WordValue += value;

            return this;
        }

        public IWord Sub(IWord word)
        {
            WordValue -= (word as Word32Bit).WordValue;

            return this;
        }

        public IWord XorWith(IWord word)
        {
            WordValue ^= (word as Word32Bit).WordValue;

            return this;
        }

        public IWord Clone()
        {
            return (IWord)MemberwiseClone();
        }

        public Int32 ToInt32()
        {
            return (Int32)WordValue;
        }
    }

    internal class Word64BitFactory : IWordFactory
    {
        public Int32 BytesPerWord => Word64Bit.BytesPerWord;

        public Int32 BytesPerBlock => BytesPerWord * 2;

        public IWord Create()
        {
            return CreateConcrete();
        }

        public IWord CreateP()
        {
            return CreateConcrete(RC5Constants.P64);
        }

        public IWord CreateQ()
        {
            return CreateConcrete(RC5Constants.Q64);
        }

        public IWord CreateFromBytes(Byte[] bytes, Int32 startFromIndex)
        {
            var word = Create();
            word.CreateFromBytes(bytes, startFromIndex);

            return word;
        }

        private Word64Bit CreateConcrete(UInt64 value = 0)
        {
            return new Word64Bit
            {
                WordValue = value
            };
        }
    }

    internal class Word64Bit : IWord
    {
        public const Int32 WordSizeInBits = BytesPerWord * RC5Constants.BitsPerByte;
        public const Int32 BytesPerWord = sizeof(UInt64);

        public UInt64 WordValue { get; set; }

        public void CreateFromBytes(Byte[] bytes, Int32 startFrom)
        {
            WordValue = 0;

            for (var i = startFrom + BytesPerWord - 1; i > startFrom; --i)
            {
                WordValue |= bytes[i];
                WordValue <<= RC5Constants.BitsPerByte;
            }

            WordValue |= bytes[startFrom];
        }

        public Byte[] FillBytesArray(Byte[] bytesToFill, Int32 startFrom)
        {
            var i = 0;
            for (; i < BytesPerWord - 1; ++i)
            {
                bytesToFill[startFrom + i] = (Byte)(WordValue & RC5Constants.ByteMask);
                WordValue >>= RC5Constants.BitsPerByte;
            }

            bytesToFill[startFrom + i] = (Byte)(WordValue & RC5Constants.ByteMask);

            return bytesToFill;
        }

        public IWord ROL(Int32 offset)
        {
            WordValue = (WordValue << offset) | (WordValue >> (WordSizeInBits - offset));

            return this;
        }

        public IWord ROR(Int32 offset)
        {
            WordValue = (WordValue >> offset) | (WordValue << (WordSizeInBits - offset));

            return this;
        }

        public IWord Add(IWord word)
        {
            WordValue += (word as Word64Bit).WordValue;

            return this;
        }

        public IWord Add(Byte value)
        {
            WordValue += value;

            return this;
        }

        public IWord Sub(IWord word)
        {
            WordValue -= (word as Word64Bit).WordValue;

            return this;
        }

        public IWord XorWith(IWord word)
        {
            WordValue ^= (word as Word64Bit).WordValue;

            return this;
        }

        public IWord Clone()
        {
            return (IWord)MemberwiseClone();
        }

        public Int32 ToInt32()
        {
            return (Int32)WordValue;
        }
    }

    public static class RSAPrivateEncryption
    {
        public static byte[] PrivateEncipher(this RSACryptoServiceProvider rsa, byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");
            if (rsa.PublicOnly)
                throw new InvalidOperationException("Private key is not loaded");

            int maxDataLength = (rsa.KeySize / 8) - 6;
            if (data.Length > maxDataLength)
                throw new ArgumentOutOfRangeException("data", string.Format(
                    "Maximum data length for the current key size ({0} bits) is {1} bytes (current length: {2} bytes)",
                    rsa.KeySize, maxDataLength, data.Length));

            BigInteger numData = GetBig(AddPadding(data));

            RSAParameters rsaParams = rsa.ExportParameters(true);
            BigInteger D = GetBig(rsaParams.D);
            BigInteger Modulus = GetBig(rsaParams.Modulus);
            BigInteger encData = BigInteger.ModPow(numData, D, Modulus);

            return encData.ToByteArray();
        }

        public static byte[] PublicDecipher(this RSACryptoServiceProvider rsa, byte[] cipherData)
        {
            if (cipherData == null)
                throw new ArgumentNullException("cipherData");

            BigInteger numEncData = new BigInteger(cipherData);

            RSAParameters rsaParams = rsa.ExportParameters(false);
            BigInteger Exponent = GetBig(rsaParams.Exponent);
            BigInteger Modulus = GetBig(rsaParams.Modulus);

            BigInteger decData = BigInteger.ModPow(numEncData, Exponent, Modulus);

            byte[] data = decData.ToByteArray();
            byte[] result = new byte[data.Length - 1];
            Array.Copy(data, result, result.Length);
            result = RemovePadding(result);

            Array.Reverse(result);
            return result;
        }

        private static BigInteger GetBig(byte[] data)
        {
            byte[] inArr = (byte[])data.Clone();
            Array.Reverse(inArr);
            byte[] final = new byte[inArr.Length + 1];
            Array.Copy(inArr, final, inArr.Length);

            return new BigInteger(final);
        }

        private static byte[] AddPadding(byte[] data)
        {
            Random rnd = new Random();
            byte[] paddings = new byte[4];
            rnd.NextBytes(paddings);
            paddings[0] = (byte)(paddings[0] | 128);

            byte[] results = new byte[data.Length + 4];

            Array.Copy(paddings, results, 4);
            Array.Copy(data, 0, results, 4, data.Length);
            return results;
        }

        private static byte[] RemovePadding(byte[] data)
        {
            byte[] results = new byte[data.Length - 4];
            Array.Copy(data, results, results.Length);
            return results;
        }
    }
}
