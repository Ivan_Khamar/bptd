﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Laba5_CryptoAPI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly DSACryptoServiceProvider _dsa = new DSACryptoServiceProvider();
        private readonly SHA1 _sha1 = SHA1.Create();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void createFromFileBtn_Click(object sender, RoutedEventArgs e)
        {
            //using (var openFileDialog = new OpenFileDialog())
            //{
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    var filePath = openFileDialog.FileName;
                    fileInputTextBox.Text = filePath;

                    ProcessSignature(File.ReadAllBytes(filePath));
                }
            //}
        }

        private void createFromTextInputBtn_Click(object sender, RoutedEventArgs e)
        {
            ProcessSignature(Encoding.Default.GetBytes(textInputTextBox.Text));
        }
        private void ProcessSignature(Byte[] message)
        {
            byte[] hash = _sha1.ComputeHash(message);
            string result = Convert.ToBase64String(_dsa.CreateSignature(hash));

            createdSignatureTextBox.Text = result.Trim();
        }

        private void saveSignatureBtn_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(createdSignatureTextBox.Text))
            {
                return;
            }

            var saveFileDialog = new SaveFileDialog
            {
                Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*",
                RestoreDirectory = true,
                FileName = "Signature.txt"
            };

            if (saveFileDialog.ShowDialog() == true)
            {
                File.WriteAllText(saveFileDialog.FileName, createdSignatureTextBox.Text);
            }
        }

        private void chooseFileBtn_Click(object sender, RoutedEventArgs e)
        {
            //using (var openFileDialog = new OpenFileDialog())
            //{
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    chosenFileTextBox.Text = openFileDialog.FileName;
                }
            //}
        }

        private void chooseSignatureFileBtn_Click(object sender, RoutedEventArgs e)
        {
            //using (var openFileDialog = new OpenFileDialog())
            //{
                OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    chosenSignatureFileTextBox.Text = openFileDialog.FileName;
                }
            //}
        }

        private void verifyBtn_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(chosenFileTextBox.Text) || string.IsNullOrWhiteSpace(chosenSignatureFileTextBox.Text))
            {
                return;
            }

            byte[] message = File.ReadAllBytes(chosenFileTextBox.Text);
            string sign = File.ReadAllText(chosenSignatureFileTextBox.Text);

            var result = VerifySignature(message, sign)
                ? "Verified"
                : "Not verified";

            MessageBox.Show(result);
        }

        private bool VerifySignature(byte[] message, string sign)
        {
            try
            {
                byte[] hash = _sha1.ComputeHash(message);
                bool verified = _dsa.VerifySignature(hash, Convert.FromBase64String(sign));
                return verified;
            }
            catch
            {
                return false;
            }
        }
    }
}
